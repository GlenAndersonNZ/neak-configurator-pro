package com.neak.configurator_pro;

import java.io.File;

import com.neak.configurator_pro.toolset.fileTools;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class NEAK_GPU_S3_Control_Activity extends Activity {
	
	private boolean isEdit = false;
	
	private Button btnIndex;
	
	private Button btnApply;
	private CheckBox chkOnBoot;
	
	private TextView tvStep0, tvStep1, tvStep2, tvStep3;
	private TextView tvStep0V, tvStep1V, tvStep2V, tvStep3V;
	
	private SeekBar sbStep0, sbStep1, sbStep2, sbStep3;
	private SeekBar sbStep0V, sbStep1V, sbStep2V, sbStep3V;
	
	int newStep0, newStep1, newStep2, newStep3;
	int newStep0V, newStep1V, newStep2V, newStep3V;
	
	final fileTools tools = new fileTools();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_gpu_s3_control_layout);
	    
	    initiateUI();
	    checkSetOnBoot();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		isEdit = false;
		initiateUI();
		checkSetOnBoot();
	}
	
	public void checkSetOnBoot() {
		File f;
		try {
			f = new File("/etc/init.d/S41neakGPU");
			if (f.exists()) {
				chkOnBoot.setChecked(true);
			}
			else {
				chkOnBoot.setChecked(false);
			}
		}
		catch (Exception e) {
			//Do Something...
		}
	}
	
	public void initiateUI(){
		btnIndex = (Button)findViewById(R.id.button_Backup_Index);
		btnIndex.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
            }
        });
		btnApply = (Button)findViewById(R.id.button_GPU_Apply);
		btnApply.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (isEdit) {
                	applySettings();
                	isEdit = false;
                	initiateUI();
                }
                else {
                	isEdit = true;
                	initiateUI();
                }
            }
        });
		chkOnBoot = (CheckBox)findViewById(R.id.check_GPU_ApplyOnBoot);
		tvStep0 = (TextView)findViewById(R.id.text_GPU_step0);
		tvStep1 = (TextView)findViewById(R.id.text_GPU_step1);
		tvStep2 = (TextView)findViewById(R.id.text_GPU_step2);
		tvStep3 = (TextView)findViewById(R.id.text_GPU_step3);
		tvStep0V = (TextView)findViewById(R.id.text_GPUV_step0);
		tvStep1V = (TextView)findViewById(R.id.text_GPUV_step1);
		tvStep2V = (TextView)findViewById(R.id.text_GPUV_step2);
		tvStep3V = (TextView)findViewById(R.id.text_GPUV_step3);
		sbStep0 = (SeekBar)findViewById(R.id.seek_GPU_step0);
		sbStep1 = (SeekBar)findViewById(R.id.seek_GPU_step1);
		sbStep2 = (SeekBar)findViewById(R.id.seek_GPU_step2);
		sbStep3 = (SeekBar)findViewById(R.id.seek_GPU_step3);
		sbStep0V = (SeekBar)findViewById(R.id.seek_GPUV_step0);
		sbStep1V = (SeekBar)findViewById(R.id.seek_GPUV_step1);
		sbStep2V = (SeekBar)findViewById(R.id.seek_GPUV_step2);
		sbStep3V = (SeekBar)findViewById(R.id.seek_GPUV_step3);
		
				
		String tmp = tools.readSystemFile("/sys/devices/virtual/misc/gpu_clock_control/gpu_control");
		int tmp1[] = parseFreqValue(tmp);
				
		switch(tmp1[0]) {
		case 100:
			tvStep0.setText("100 MHz");
			sbStep0.setProgress(0);
			newStep0 = 100;
			break;
		case 133:
			tvStep0.setText("133 MHz");
			sbStep0.setProgress(1);
			newStep0 = 133;
			break;
		case 160:
			tvStep0.setText("160 MHz");
			sbStep0.setProgress(2);
			newStep0 = 160;
			break;
		case 266:
			tvStep0.setText("266 MHz");
			sbStep0.setProgress(3);
			newStep0 = 266;
			break;
		case 350:
			tvStep0.setText("350 MHz");
			sbStep0.setProgress(4);
			newStep0 = 350;
			break;
		case 440:
			tvStep0.setText("440 MHz");
			sbStep0.setProgress(5);
			newStep0 = 440;
			break;
		default:
			tvStep0.setText("Unsupported - Changed to 160MHz");
			sbStep0.setProgress(2);
			newStep0 = 160;
			break;
		}
		
		sbStep0.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					switch(progress) {
					case 0:
						tvStep0.setText("100 MHz");
						tvStep0.refreshDrawableState();
						newStep0 = 100;
						break;
					case 1:
						tvStep0.setText("133 MHz");
						tvStep0.refreshDrawableState();
						newStep0 = 133;
						break;
					case 2:
						tvStep0.setText("160 MHz");
						tvStep0.refreshDrawableState();
						newStep0 = 160;
						break;	
					case 3:
						tvStep0.setText("266 MHz");
						tvStep0.refreshDrawableState();
						newStep0 = 266;
						break;
					case 4:
						tvStep0.setText("350 MHz");
						tvStep0.refreshDrawableState();
						newStep0 = 350;
						break;
					case 5:
						tvStep0.setText("440 MHz");
						tvStep0.refreshDrawableState();
						newStep0 = 440;
						break;
					default:
						break;
					}
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		switch(tmp1[1]) {
		case 100:
			tvStep1.setText("100 MHz");
			sbStep1.setProgress(0);
			newStep1 = 100;
			break;
		case 133:
			tvStep1.setText("133 MHz");
			sbStep1.setProgress(1);
			newStep1 = 133;
			break;
		case 160:
			tvStep1.setText("160 MHz");
			sbStep1.setProgress(2);
			newStep1 = 160;
			break;
		case 266:
			tvStep1.setText("266 MHz");
			sbStep1.setProgress(3);
			newStep1 = 266;
			break;
		case 350:
			tvStep1.setText("350 MHz");
			sbStep1.setProgress(4);
			newStep1 = 350;
			break;
		case 440:
			tvStep1.setText("440 MHz");
			sbStep1.setProgress(5);
			newStep1 = 440;
			break;
		default:
			tvStep1.setText("Unsupported - Changed to 266MHz");
			sbStep1.setProgress(3);
			newStep1 = 266;
			break;
		}
		
		sbStep1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					switch(progress) {
					case 0:
						tvStep1.setText("100 MHz");
						tvStep1.refreshDrawableState();
						newStep1 = 100;
						break;
					case 1:
						tvStep1.setText("133 MHz");
						tvStep1.refreshDrawableState();
						newStep1 = 133;
						break;
					case 2:
						tvStep1.setText("160 MHz");
						tvStep1.refreshDrawableState();
						newStep1 = 160;
						break;	
					case 3:
						tvStep1.setText("266 MHz");
						tvStep1.refreshDrawableState();
						newStep1 = 266;
						break;
					case 4:
						tvStep1.setText("350 MHz");
						tvStep1.refreshDrawableState();
						newStep1 = 350;
						break;
					case 5:
						tvStep1.setText("440 MHz");
						tvStep1.refreshDrawableState();
						newStep1 = 440;
						break;
					default:
						break;
					}
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		switch(tmp1[2]) {
		case 100:
			tvStep2.setText("100 MHz");
			sbStep2.setProgress(0);
			newStep2 = 100;
			break;
		case 133:
			tvStep2.setText("133 MHz");
			sbStep2.setProgress(1);
			newStep2 = 133;
			break;
		case 160:
			tvStep2.setText("160 MHz");
			sbStep2.setProgress(2);
			newStep2 = 160;
			break;
		case 266:
			tvStep2.setText("266 MHz");
			sbStep2.setProgress(3);
			newStep2 = 266;
			break;
		case 350:
			tvStep2.setText("350 MHz");
			sbStep2.setProgress(4);
			newStep2 = 350;
			break;
		case 440:
			tvStep2.setText("440 MHz");
			sbStep2.setProgress(5);
			newStep2 = 440;
			break;
		default:
			tvStep2.setText("Unsupported - Changed to 350MHz");
			sbStep2.setProgress(4);
			newStep2 = 350;
			break;
		}
		
		sbStep2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					switch(progress) {
					case 0:
						tvStep2.setText("100 MHz");
						tvStep2.refreshDrawableState();
						newStep2 = 100;
						break;
					case 1:
						tvStep2.setText("133 MHz");
						tvStep2.refreshDrawableState();
						newStep2 = 133;
						break;
					case 2:
						tvStep2.setText("160 MHz");
						tvStep2.refreshDrawableState();
						newStep2 = 160;
						break;	
					case 3:
						tvStep2.setText("266 MHz");
						tvStep2.refreshDrawableState();
						newStep2 = 266;
						break;
					case 4:
						tvStep2.setText("350 MHz");
						tvStep2.refreshDrawableState();
						newStep2 = 350;
						break;
					case 5:
						tvStep2.setText("440 MHz");
						tvStep2.refreshDrawableState();
						newStep2 = 440;
						break;
					default:
						break;
					}
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		switch(tmp1[3]) {
		case 100:
			tvStep3.setText("100 MHz");
			sbStep3.setProgress(0);
			newStep3 = 100;
			break;
		case 133:
			tvStep3.setText("133 MHz");
			sbStep3.setProgress(1);
			newStep3 = 133;
			break;
		case 160:
			tvStep3.setText("160 MHz");
			sbStep3.setProgress(2);
			newStep3 = 160;
			break;
		case 266:
			tvStep3.setText("266 MHz");
			sbStep3.setProgress(3);
			newStep3 = 266;
			break;
		case 350:
			tvStep3.setText("350 MHz");
			sbStep3.setProgress(4);
			newStep3 = 350;
			break;
		case 440:
			tvStep3.setText("440 MHz");
			sbStep3.setProgress(5);
			newStep3 = 440;
			break;
		default:
			tvStep3.setText("Unsupported - Changed to 440MHz");
			sbStep3.setProgress(5);
			newStep3 = 440;
			break;
		}
		
		sbStep3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					switch(progress) {
					case 0:
						tvStep3.setText("100 MHz");
						tvStep3.refreshDrawableState();
						newStep3 = 100;
						break;
					case 1:
						tvStep3.setText("133 MHz");
						tvStep3.refreshDrawableState();
						newStep3 = 133;
						break;
					case 2:
						tvStep3.setText("160 MHz");
						tvStep3.refreshDrawableState();
						newStep3 = 160;
						break;	
					case 3:
						tvStep3.setText("266 MHz");
						tvStep3.refreshDrawableState();
						newStep3 = 266;
						break;
					case 4:
						tvStep3.setText("350 MHz");
						tvStep3.refreshDrawableState();
						newStep3 = 350;
						break;
					case 5:
						tvStep3.setText("440 MHz");
						tvStep3.refreshDrawableState();
						newStep3 = 440;
						break;
					default:
						break;
					}
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		tmp = tools.readSystemFile("/sys/devices/virtual/misc/gpu_voltage_control/gpu_control");
		int[] tmp1v = parseVoltValue(tmp);
		
		int tmp2v = tmp1v[0] / 1000;
		tvStep0V.setText("Unsupported Voltage - set to 875 mV");
		sbStep0V.setProgress(11);
		newStep0V = 875000;
		for (int i=0; i < 25; i++) {
			int test = i * 25 + 600;
			if (test == tmp2v) {
				tvStep0V.setText(tmp2v + " mV");
				sbStep0V.setProgress(i);
				newStep0V = tmp2v * 1000;
			}
		}
		
		sbStep0V.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					int newVal = progress * 25 + 600;
					tvStep0V.setText(newVal + " mV");
					newStep0V = newVal * 1000;
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		tmp2v = tmp1v[1] / 1000;
		tvStep1V.setText("Unsupported Voltage - set to 900 mV");
		sbStep1V.setProgress(12);
		newStep1V = 900000;
		for (int i=0; i < 25; i++) {
			int test = i * 25 + 600;
			if (test == tmp2v) {
				tvStep1V.setText(tmp2v + " mV");
				sbStep1V.setProgress(i);
				newStep1V = tmp2v * 1000;
			}
		}
		
		sbStep1V.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					int newVal = progress * 25 + 600;
					tvStep1V.setText(newVal + " mV");
					newStep1V = newVal * 1000;
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		tmp2v = tmp1v[2] / 1000;
		tvStep2V.setText("Unsupported Voltage - set to 950 mV");
		sbStep2V.setProgress(14);
		newStep2V = 950000;
		for (int i=0; i < 25; i++) {
			int test = i * 25 + 600;
			if (test == tmp2v) {
				tvStep2V.setText(tmp2v + " mV");
				sbStep2V.setProgress(i);
				newStep2V = tmp2v * 1000;
			}
		}
		
		sbStep2V.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					int newVal = progress * 25 + 600;
					tvStep2V.setText(newVal + " mV");
					newStep2V = newVal * 1000;
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		tmp2v = tmp1v[3] / 1000;
		tvStep3V.setText("Unsupported Voltage - set to 1025 mV");
		sbStep3V.setProgress(17);
		newStep3V = 1025000;
		for (int i=0; i < 25; i++) {
			int test = i * 25 + 600;
			if (test == tmp2v) {
				tvStep3V.setText(tmp2v + " mV");
				sbStep3V.setProgress(i);
				newStep3V = tmp2v * 1000;
			}
		}
		
		sbStep3V.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					int newVal = progress * 25 + 600;
					tvStep3V.setText(newVal + " mV");
					newStep3V = newVal * 1000;
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		if (isEdit) {
			btnApply.setText("Apply Settings");
			chkOnBoot.setEnabled(true);
			sbStep0.setEnabled(true);
			sbStep1.setEnabled(true);
			sbStep2.setEnabled(true);
			sbStep3.setEnabled(true);
			sbStep0V.setEnabled(true);
			sbStep1V.setEnabled(true);
			sbStep2V.setEnabled(true);
			sbStep3V.setEnabled(true);
		}
		else {
			btnApply.setText("Edit Settings");
			chkOnBoot.setEnabled(false);
			sbStep0.setEnabled(false);
			sbStep1.setEnabled(false);
			sbStep2.setEnabled(false);
			sbStep3.setEnabled(false);
			sbStep0V.setEnabled(false);
			sbStep1V.setEnabled(false);
			sbStep2V.setEnabled(false);
			sbStep3V.setEnabled(false);
		}
		
	}
	
	private int[] parseFreqValue(String toParse) {
		int[] values = new int[4];
		values[0] = 0; values[1] = 0; values[2] = 0; values[3] = 0;
		try {
			String temp;
			String parsedString = toParse.substring(toParse.indexOf("0: ") + 3, toParse.indexOf("Step1"));
			temp = parsedString.replaceAll("[^0-9]","");
			values[0] = Integer.parseInt(temp);
			parsedString = toParse.substring(toParse.indexOf("1: ") + 3, toParse.indexOf("Step2"));
			temp = parsedString.replaceAll("[^0-9]","");
			values[1] = Integer.parseInt(temp);
			parsedString = toParse.substring(toParse.indexOf("2: ") + 3, toParse.indexOf(" Step3"));
			temp = parsedString.replaceAll("[^0-9]","");
			values[2] = Integer.parseInt(temp);
			parsedString = toParse.substring(toParse.indexOf("3: ") + 3, toParse.indexOf("Threshold0"));
			temp = parsedString.replaceAll("[^0-9]","");
			values[3] = Integer.parseInt(temp);
			
			return values;
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return values;

	}
	
	private int[] parseVoltValue(String toParse) {
		int[] values = new int[4];
		values[0] = 0; values[1] = 0; values[2] = 0; values[3] = 0;
		try {
			String temp;
			String parsedString = toParse.substring(toParse.indexOf("1: ") + 3, toParse.indexOf("Step2"));
			temp = parsedString.replaceAll("[^0-9]","");
			values[0] = Integer.parseInt(temp);
			parsedString = toParse.substring(toParse.indexOf("2: ") + 3, toParse.indexOf("Step3"));
			temp = parsedString.replaceAll("[^0-9]","");
			values[1] = Integer.parseInt(temp);
			parsedString = toParse.substring(toParse.indexOf("3: ") + 3, toParse.indexOf("Step4"));
			temp = parsedString.replaceAll("[^0-9]","");
			values[2] = Integer.parseInt(temp);
			parsedString = toParse.substring(toParse.indexOf("4: ") + 3);
			temp = parsedString.replaceAll("[^0-9]","");
			values[3] = Integer.parseInt(temp);
			return values;
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return values;
	}
		
	public void applySettings() {
		String cmd;
		cmd = "echo " + newStep0 + " " + newStep1 + " " + newStep2 + " " + newStep3 + " > /sys/devices/virtual/misc/gpu_clock_control/gpu_control";
		tools.writeToFile(cmd);
		cmd = "echo " + newStep0V + " " + newStep1V + " " + newStep2V + " " + newStep3V + " > /sys/devices/virtual/misc/gpu_voltage_control/gpu_control";
		tools.writeToFile(cmd);		
		if (chkOnBoot.isChecked()) {
			String data = "#!/system/bin/sh\n"
					+ "echo \\\"" + newStep0 + " " + newStep1 + " " + newStep2 + " " + newStep3 + "\\\" > /sys/devices/virtual/misc/gpu_clock_control/gpu_control\n"
					+ "echo \\\"" + newStep0V + " " + newStep1V + " " + newStep2V + " " + newStep3V + "\\\" > /sys/devices/virtual/misc/gpu_voltage_control/gpu_control\n"; 
			cmd = "echo \"" + data + "\" > /etc/init.d/S41neakGPU";
			tools.writeToFile(cmd);
		}
		else {
			cmd = "rm -f /etc/init.d/S41neakGPU";
			tools.writeToFile(cmd);
		}
		cmd = null;
	}
	
	@Override
	public void onBackPressed() {
		Toast.makeText(NEAK_GPU_S3_Control_Activity.this, "Press back once more to exit", Toast.LENGTH_SHORT).show();
		NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
	}
	
	public void backupSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS2", 0);
		SharedPreferences.Editor ed = prefs.edit();
		ed.putInt("newStep0", newStep0);
		ed.putInt("newStep1", newStep1);
		ed.putInt("newStep2", newStep2);
		ed.putInt("newStep3", newStep3);
		ed.putInt("newStep0V", newStep0V);
		ed.putInt("newStep1V", newStep1V);
		ed.putInt("newStep2V", newStep2V);
		ed.putInt("newStep3V", newStep3V);
		ed.putBoolean("chkOnBoot", chkOnBoot.isChecked());
		ed.commit();
	}
	
	public void restoreSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS2", 0);
		if (prefs.getInt("newStep0", 0) != 0 && prefs.getInt("newStep1", 0) != 0) {
			newStep0 = prefs.getInt("newStep0", newStep0);
			newStep1 = prefs.getInt("newStep1", newStep1);
			newStep2 = prefs.getInt("newStep2", newStep2);
			newStep3 = prefs.getInt("newStep3", newStep3);
			newStep0V = prefs.getInt("newStep0V", newStep0V);
			newStep1V = prefs.getInt("newStep1V", newStep1V);
			newStep2V = prefs.getInt("newStep2V", newStep2V);
			newStep3V = prefs.getInt("newStep3V", newStep3V);
			chkOnBoot.setChecked(prefs.getBoolean("chkOnBoot", chkOnBoot.isChecked()));
			applySettings();
		}
	}


}
