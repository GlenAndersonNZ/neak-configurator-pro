package com.neak.configurator_pro;

import java.io.File;

import com.neak.configurator_pro.toolset.fileTools;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class NEAK_Misc_Options_S3_Activity extends Activity {

	/** Called when the activity is first created. */
	
	private boolean isEdit = false;
	
	private Button btnIndex;
	private Button btnApply;
	private CheckBox chkOnBoot;
	
	public int newAC, newCDP, newUSB, newDock;
	
	private SeekBar sbAC, sbCDP, sbUSB, sbDock;
	private TextView tvAC, tvCDP, tvUSB, tvDock;
	
	final fileTools tools = new fileTools();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_misc_options_s3_layout);
	    
	    initiateUI();
	    checkSetOnBoot();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		isEdit = false;
		initiateUI();
		checkSetOnBoot();
	}
	
	public void checkSetOnBoot() {
		File f;
		try {
			f = new File("/etc/init.d/S44neakMisc");
			if (f.exists()) {
				chkOnBoot.setChecked(true);
			}
			else {
				chkOnBoot.setChecked(false);
			}
		}
		catch (Exception e) {
			//Do Something...
		}
	}
		
	public void initiateUI(){
		btnIndex = (Button)findViewById(R.id.button_Battery_Index);
		btnIndex.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
            }
        });
		btnApply = (Button)findViewById(R.id.button_Misc_Apply);
		btnApply.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (isEdit) {
                	applySettings();
                	isEdit = false;
                	initiateUI();
                }
                else {
                	isEdit = true;
                	initiateUI();
                }
            }
        });
		
		chkOnBoot = (CheckBox)findViewById(R.id.check_Misc_OnBoot);
		
		tvAC = (TextView)findViewById(R.id.tv_charge_ac_display);
		sbAC = (SeekBar)findViewById(R.id.sb_misc_ac);
		sbAC.setMax(32);
		sbAC.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					newAC = progress * 25 + 400;
					tvAC.setText(newAC + " mA");
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		tvCDP = (TextView)findViewById(R.id.tv_charge_cdp_display);
		sbCDP = (SeekBar)findViewById(R.id.sb_misc_cdp);
		sbCDP.setMax(32);
		sbCDP.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					newCDP = progress * 25 + 400;
					tvCDP.setText(newCDP + " mA");
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		tvUSB = (TextView)findViewById(R.id.tv_charge_usb_display);
		sbUSB = (SeekBar)findViewById(R.id.sb_misc_usb);
		sbUSB.setMax(32);
		sbUSB.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					newUSB = progress * 25 + 400;
					tvUSB.setText(newUSB + " mA");
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		tvDock = (TextView)findViewById(R.id.tv_charge_dock_display);
		sbDock = (SeekBar)findViewById(R.id.sb_misc_dock);
		sbDock.setMax(32);
		sbDock.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					newDock = progress * 25 + 400;
					tvDock.setText(newDock + " mA");
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		setChargeCurrents();
				
		if (isEdit) {
			btnApply.setText("Apply Settings");
			chkOnBoot.setEnabled(true);
			sbAC.setEnabled(true);
			sbCDP.setEnabled(true);
			sbUSB.setEnabled(true);
			sbDock.setEnabled(true);
		}
		else {
			btnApply.setText("Edit Settings");
			chkOnBoot.setEnabled(false);
			sbAC.setEnabled(false);
			sbCDP.setEnabled(false);
			sbUSB.setEnabled(false);
			sbDock.setEnabled(false);
		}

		
	}
	
	public void setChargeCurrents() {
		String getAC;
		String getCDP;
		String getUSB;
		String getDock;
		String tmp;
		int temp;
		File f = new File("/sys/devices/virtual/misc/charge_current/charge_current");
		if (f.exists()) {
			String file = tools.readSystemFile("/sys/devices/virtual/misc/charge_current/charge_current");
			
			try {
				tmp = file.substring(file.indexOf("AC"), file.indexOf("CDP"));
				getAC = tmp.substring(tmp.indexOf(":") + 2);
				newAC = Integer.parseInt(getAC.replaceAll("[^0-9]",""));
			}
			catch (NullPointerException e) {
				Log.e("EXCEPTION: ", e.toString());
				getAC = "400";
			}
			temp = (newAC - 400) / 25;
			sbAC.setProgress(temp);
			tvAC.setText(newAC + " mA");
			
			try {
				tmp = file.substring(file.indexOf("CDP"), file.indexOf("USB"));
				getCDP = tmp.substring(tmp.indexOf(":") + 2);
				newCDP = Integer.parseInt(getCDP.replaceAll("[^0-9]",""));
			}
			catch (NullPointerException e) {
				Log.e("EXCEPTION: ", e.toString());
				getCDP = "400";
			}
			temp = (newCDP - 400) / 25;
			sbCDP.setProgress(temp);
			tvCDP.setText(newCDP + " mA");
			
			try {
				tmp = file.substring(file.indexOf("USB"), file.indexOf("Dock"));
				getUSB = tmp.substring(tmp.indexOf(":") + 2);
				newUSB = Integer.parseInt(getUSB.replaceAll("[^0-9]",""));
			}
			catch (NullPointerException e) {
				Log.e("EXCEPTION: ", e.toString());
				getUSB = "400";
			}
			temp = (newUSB - 400) / 25;
			sbUSB.setProgress(temp);
			tvUSB.setText(newUSB + " mA");
			
			try {
				tmp = file.substring(file.indexOf("Dock"));
				getDock = tmp.substring(tmp.indexOf(":") + 2);
				newDock = Integer.parseInt(getDock.replaceAll("[^0-9]",""));
			}
			catch (NullPointerException e) {
				Log.e("EXCEPTION: ", e.toString());
				getDock = "400";
			}
			temp = (newDock - 400) / 25;
			sbDock.setProgress(temp);
			tvDock.setText(newDock + " mA");
		}
		
	}
	

	public void applySettings() {
		String cmd;
		String values = newAC 
				+ " " + newCDP 
				+ " " + newUSB
				+ " " + newDock;
		cmd = "echo \"" + values + "\" > /sys/devices/virtual/misc/charge_current/charge_current";
		tools.writeToFile(cmd);
		if (chkOnBoot.isChecked()) {
			StringBuilder s = new StringBuilder();
			s.append("#!/system/bin/sh\n");
			s.append("echo \\\"" + values + "\\\" > /sys/devices/virtual/misc/charge_current/charge_current\n");
			cmd = "echo \"" + s.toString() + "\" > /etc/init.d/S44neakMisc";
			tools.writeToFile(cmd);
		}
		else {
			cmd = "rm -f /etc/init.d/S44neakMisc";
			tools.writeToFile(cmd);
		}
	}
	
	@Override
	public void onBackPressed() {
		Toast.makeText(NEAK_Misc_Options_S3_Activity.this, "Press back once more to exit", Toast.LENGTH_SHORT).show();
		NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
	}
	
	public void backupSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS4", 0);
		SharedPreferences.Editor ed = prefs.edit();
		ed.putInt("newAC", newAC);
		ed.putInt("newCDP", newCDP);
		ed.putInt("newUSB", newUSB);
		ed.putInt("newDock", newDock);
		ed.putBoolean("chkOnBoot", chkOnBoot.isChecked());
		ed.commit();
	}
	
	public void restoreSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS4", 0);
		if (prefs.getInt("newAC", 0) != 0 && prefs.getInt("newCDP", 0) != 0) {
			newAC = prefs.getInt("newAC", newAC);
			newCDP = prefs.getInt("newCDP", newCDP);
			newUSB = prefs.getInt("newUSB", newUSB);
			newDock = prefs.getInt("newDock", newDock);
			chkOnBoot.setChecked(prefs.getBoolean("chkOnBoot", chkOnBoot.isChecked()));
			applySettings();
		}
	}

}
