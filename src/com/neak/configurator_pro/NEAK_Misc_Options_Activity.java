package com.neak.configurator_pro;

import java.io.File;
import java.util.ArrayList;

import com.neak.configurator_pro.toolset.fileTools;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class NEAK_Misc_Options_Activity extends Activity {

	/** Called when the activity is first created. */
	
	private boolean isEdit = false;
	
	private Button btnIndex;
	private Button btnApply;
	private CheckBox chkOnBoot;
	
	private Spinner spinSecondCore;
	private ArrayAdapter<String> adapterSecondCore;
	
	private EditText editLoadL;
	private EditText editLoadH;
	
	private CheckBox chkAC450;
	private CheckBox chkAC650;
	private CheckBox chkMisc450;
	private CheckBox chkMisc650;
	private CheckBox chkUSB450;
	private CheckBox chkUSB650;
	
	private RelativeLayout dualCoreLayout;
	private RelativeLayout chargeLayout;
	
	public int newAC, newMisc, newUSB, newLoadL, newLoadH;
	
	public String newHotplug = "on";
	public String newSecondCore = "off";
	
	public String LoadLLoc;
	public String LoadHLoc;
	
	final fileTools tools = new fileTools();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_misc_options_layout);
	    
	    initiateUI();
	    checkSetOnBoot();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		isEdit = false;
		initiateUI();
		checkSetOnBoot();
	}
	
	public void checkSetOnBoot() {
		File f;
		try {
			f = new File("/etc/init.d/S44neakMisc");
			if (f.exists()) {
				chkOnBoot.setChecked(true);
			}
			else {
				chkOnBoot.setChecked(false);
			}
		}
		catch (Exception e) {
			//Do Something...
		}
	}
		
	public void initiateUI(){
		dualCoreLayout = (RelativeLayout)findViewById(R.id.Misc_DualCore_Layout);
		chargeLayout = (RelativeLayout)findViewById(R.id.Misc_Charge_Layout);
		dualCoreLayout.setVisibility(View.VISIBLE);
		chargeLayout.setVisibility(View.VISIBLE);
		btnIndex = (Button)findViewById(R.id.button_Battery_Index);
		btnIndex.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
            }
        });
		btnApply = (Button)findViewById(R.id.button_Misc_Apply);
		btnApply.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (isEdit) {
                	applySettings();
                	isEdit = false;
                	initiateUI();
                }
                else {
                	isEdit = true;
                	initiateUI();
                }
            }
        });
		
		chkOnBoot = (CheckBox)findViewById(R.id.check_Misc_OnBoot);
		
		spinSecondCore = (Spinner) findViewById(R.id.spinner_Misc_DualCore);
		ArrayList<String> secondCoreArray = new ArrayList<String>();
		secondCoreArray.add("Dynamic Hotplug");
		secondCoreArray.add("Single Core Mode");
		secondCoreArray.add("Dual Core Mode");
		adapterSecondCore = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, secondCoreArray);
		adapterSecondCore.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinSecondCore.setAdapter(adapterSecondCore);
		spinSecondCore.setSelection(adapterSecondCore.getPosition(getSecondCoreSetting()));
		
		editLoadL = (EditText)findViewById(R.id.edit_Misc_DualCore_Loadl);
		editLoadH = (EditText)findViewById(R.id.edit_Misc_DualCore_Loadh);
		
		getSecondCoreLoadSetting();
		
		spinSecondCore.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
		    	if (spinSecondCore.getSelectedItemPosition() == adapterSecondCore.getPosition("Dynamic Hotplug")) {
					if (isEdit) {
						editLoadL.setEnabled(true);
						editLoadH.setEnabled(true);
					}
					newHotplug = "on";
					newSecondCore = "off";
				}
				else if (spinSecondCore.getSelectedItemPosition() == adapterSecondCore.getPosition("Single Core Mode")){
					editLoadL.setEnabled(false);
					editLoadH.setEnabled(false);
					newHotplug = "off";
					newSecondCore = "off";
				}
				else if (spinSecondCore.getSelectedItemPosition() == adapterSecondCore.getPosition("Dual Core Mode")){
					editLoadL.setEnabled(false);
					editLoadH.setEnabled(false);
					newHotplug = "off";
					newSecondCore = "on";
				}
		    } 

		    public void onNothingSelected(AdapterView<?> adapterView) {
		        return;
		    } 
		}); 
		
		chkAC450 = (CheckBox)findViewById(R.id.check_Misc_Charge_AC_450);
		chkAC650 = (CheckBox)findViewById(R.id.check_Misc_Charge_AC_650);
		chkMisc450 = (CheckBox)findViewById(R.id.check_Misc_Charge_Misc_450);
		chkMisc650 = (CheckBox)findViewById(R.id.check_Misc_Charge_Misc_650);
		chkUSB450 = (CheckBox)findViewById(R.id.check_Misc_Charge_USB_450);
		chkUSB650 = (CheckBox)findViewById(R.id.check_Misc_Charge_USB_650);
		
		setChargeCurrents();
		
		chkAC450.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					chkAC650.setChecked(false);
					newAC = 450;
				}
				else if (!isChecked) {
					chkAC650.setChecked(true);
					newAC = 650;
				}
			}
		});
		chkAC650.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					chkAC450.setChecked(false);
					newAC = 650;
				}
				else if (!isChecked) {
					chkAC450.setChecked(true);
					newAC = 450;
				}
			}
		});
		chkMisc450.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					chkMisc650.setChecked(false);
					newMisc = 450;
				}
				else if (!isChecked) {
					chkMisc650.setChecked(true);
					newMisc = 650;
				}
			}
		});
		chkMisc650.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					chkMisc450.setChecked(false);
					newMisc = 650;
				}
				else if (!isChecked) {
					chkMisc450.setChecked(true);
					newMisc = 450;
				}
			}
		});
		chkUSB450.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					chkUSB650.setChecked(false);
					newUSB = 450;
				}
				else if (!isChecked) {
					chkUSB650.setChecked(true);
					newUSB = 650;
				}
			}
		});
		chkUSB650.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					chkUSB450.setChecked(false);
					newUSB = 650;
				}
				else if (!isChecked) {
					chkUSB450.setChecked(true);
					newUSB = 450;
				}
			}
		});
		
		if (isEdit) {
			btnApply.setText("Apply Settings");
			chkOnBoot.setEnabled(true);
			spinSecondCore.setEnabled(true);
			if (spinSecondCore.getSelectedItemPosition() == adapterSecondCore.getPosition("Dynamic Hotplug")) {
				editLoadL.setEnabled(true);
				editLoadH.setEnabled(true);
			}
			chkAC450.setEnabled(true);
			chkAC650.setEnabled(true);
			chkMisc450.setEnabled(true);
			chkMisc650.setEnabled(true);
			chkUSB450.setEnabled(true);
			chkUSB650.setEnabled(true);
		}
		else {
			btnApply.setText("Edit Settings");
			chkOnBoot.setEnabled(false);
			spinSecondCore.setEnabled(false);
			editLoadL.setEnabled(false);
			editLoadH.setEnabled(false);
			chkAC450.setEnabled(false);
			chkAC650.setEnabled(false);
			chkMisc450.setEnabled(false);
			chkMisc650.setEnabled(false);
			chkUSB450.setEnabled(false);
			chkUSB650.setEnabled(false);
		}

		
	}
	
	public void setChargeCurrents() {
		String getAC;
		String getMisc;
		String getUSB;
		String tmp;
		File f = new File("/sys/devices/virtual/misc/charge_current/charge_current");
		if (f.exists()) {
			String file = tools.readSystemFile("/sys/devices/virtual/misc/charge_current/charge_current");
			
			try {
				tmp = file.substring(file.indexOf("A"), file.indexOf("M"));
				getAC = tmp.substring(tmp.indexOf(":") + 2);
			}
			catch (NullPointerException e) {
				Log.e("EXCEPTION: ", e.toString());
				getAC = "0";
			}
			newAC = Integer.parseInt(getAC);
			switch (newAC) {
			case 450:
				chkAC450.setChecked(true);
				chkAC650.setChecked(false);
				break;
			case 650:
				chkAC450.setChecked(false);
				chkAC650.setChecked(true);
				break;
			default:
				if (newAC >= 650) {
					chkAC450.setChecked(false);
					chkAC650.setChecked(true);
					newAC = 650;
				}
				else {
					chkAC650.setChecked(false);
					chkAC450.setChecked(true);
					newAC = 450;
				}
				break;
			}
			
			try {
				tmp = file.substring(file.indexOf("M"), file.indexOf("U"));
				getMisc = tmp.substring(tmp.indexOf(":") + 2);
			}
			catch (NullPointerException e) {
				Log.e("EXCEPTION: ", e.toString());
				getMisc = "0";
			}
			newMisc = Integer.parseInt(getMisc);
			switch (newMisc) {
			case 450:
				chkMisc450.setChecked(true);
				chkMisc650.setChecked(false);
				break;
			case 650:
				chkMisc450.setChecked(false);
				chkMisc650.setChecked(true);
				break;
			default:
				if (newMisc >= 650) {
					chkMisc450.setChecked(false);
					chkMisc650.setChecked(true);
					newMisc = 650;
				}
				else {
					chkMisc650.setChecked(false);
					chkMisc450.setChecked(true);
					newMisc = 450;
				}
				break;
			}
			
			try {
				tmp = file.substring(file.indexOf("U"));
				getUSB = tmp.substring(tmp.indexOf(":") + 2);
			}
			catch (NullPointerException e) {
				Log.e("EXCEPTION: ", e.toString());
				getUSB = "0";
			}
			newUSB = Integer.parseInt(getUSB);
			switch (newUSB) {
			case 450:
				chkUSB450.setChecked(true);
				chkUSB650.setChecked(false);
				break;
			case 650:
				chkUSB450.setChecked(false);
				chkUSB650.setChecked(true);
				break;
			default:
				if (newUSB >= 650) {
					chkUSB450.setChecked(false);
					chkUSB650.setChecked(true);
					newUSB = 650;
				}
				else {
					chkUSB650.setChecked(false);
					chkUSB450.setChecked(true);
					newUSB = 450;
				}
				break;
			}
		}
		else {
			chargeLayout.setVisibility(View.GONE);
		}
		
		
	}
	
	public String getSecondCoreSetting() {
		File f = new File("/sys/devices/virtual/misc/second_core/hotplug_on");
		if (f.exists()) {
			if (tools.readSystemFile("/sys/devices/virtual/misc/second_core/hotplug_on").equalsIgnoreCase("on")) {
				newHotplug = "on";
				newSecondCore = "off";
				return "Dynamic Hotplug";
			}
			else {
				if (tools.readSystemFile("/sys/devices/virtual/misc/second_core/second_core_on").equalsIgnoreCase("on")) {
					newHotplug = "off";
					newSecondCore = "on";
					return "Dual Core Mode";
				}
				else {
					newHotplug = "off";
					newSecondCore = "off";
					return "Single Core Mode";
				}
			}
		}
		else {
			dualCoreLayout.setVisibility(View.GONE);
			return "Single Core Mode";
		}
	}
	
	public void getSecondCoreLoadSetting() {
		//File f = new File("/init.smdk4210.rc");
		File f = new File("/sys/module/stand_hotplug/parameters/load_l1");
		File f1 = new File("/sys/module/pm_hotplug/parameters/loadl");
		if (f.exists() | f1.exists()) {
			if (f.exists()){
				newLoadL = Integer.parseInt(tools.readSystemFile("/sys/module/stand_hotplug/parameters/load_l1"));
				editLoadL.setText(newLoadL + "");
				LoadLLoc = "/sys/module/stand_hotplug/parameters/load_l1";
				newLoadH = Integer.parseInt(tools.readSystemFile("/sys/module/stand_hotplug/parameters/load_h0"));
				editLoadH.setText(newLoadH + "");
				LoadHLoc = "/sys/module/stand_hotplug/parameters/load_h0";
			}
			else {
				newLoadL = Integer.parseInt(tools.readSystemFile("/sys/module/pm_hotplug/parameters/loadl"));
				editLoadL.setText(newLoadL + "");
				LoadLLoc = "/sys/module/pm_hotplug/parameters/loadl";
				newLoadH = Integer.parseInt(tools.readSystemFile("/sys/module/pm_hotplug/parameters/loadh"));
				editLoadH.setText(newLoadH + "");
				LoadHLoc = "/sys/module/pm_hotplug/parameters/loadh";
			}
		}
		else {
			dualCoreLayout.setVisibility(View.GONE);
		}
	}
	
	public void applySettings() {
		String cmd;
		String values = newAC 
				+ " " + newMisc 
				+ " " + newUSB;
		if (!(dualCoreLayout.getVisibility() == View.GONE)) {
			newLoadL = Integer.parseInt(editLoadL.getText().toString());
			newLoadH = Integer.parseInt(editLoadH.getText().toString());
			cmd = "echo \"" + newHotplug + "\" > /sys/devices/virtual/misc/second_core/hotplug_on";
			tools.writeToFile(cmd);
			cmd = "echo \"" + newSecondCore + "\" > /sys/devices/virtual/misc/second_core/second_core_on";
			tools.writeToFile(cmd);
			cmd = "echo \"" + newLoadL + "\" > " + LoadLLoc;
			tools.writeToFile(cmd);
			cmd = "echo \"" + newLoadH + "\" > " + LoadHLoc;
			tools.writeToFile(cmd);
		}
		if (!(chargeLayout.getVisibility() == View.GONE)) {
			cmd = "echo \"" + values + "\" > /sys/devices/virtual/misc/charge_current/charge_current";
			tools.writeToFile(cmd);
		}
		if (chkOnBoot.isChecked()) {
			StringBuilder s = new StringBuilder();
			s.append("#!/system/bin/sh\n");
			if (!(dualCoreLayout.getVisibility() == View.GONE)) {
				s.append( "echo \\\"" + newHotplug + "\\\" > /sys/devices/virtual/misc/second_core/hotplug_on\n"
				+ "echo \\\"" + newSecondCore + "\\\" > /sys/devices/virtual/misc/second_core/second_core_on\n"
				+ "echo \\\"" + newLoadL + "\\\" > " + LoadLLoc + "\n"
				+ "echo \\\"" + newLoadH + "\\\" > " + LoadHLoc + "\n");
			}
			if (!(chargeLayout.getVisibility() == View.GONE)) {
				s.append("echo \\\"" + values + "\\\" > /sys/devices/virtual/misc/charge_current/charge_current\n");
			}
			cmd = "echo \"" + s.toString() + "\" > /etc/init.d/S44neakMisc";
			tools.writeToFile(cmd);
		}
		else {
			cmd = "rm -f /etc/init.d/S44neakMisc";
			tools.writeToFile(cmd);
		}
	}
	
	@Override
	public void onBackPressed() {
		Toast.makeText(NEAK_Misc_Options_Activity.this, "Press back once more to exit", Toast.LENGTH_SHORT).show();
		NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
	}
	
	public void backupSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS3", 0);
		SharedPreferences.Editor ed = prefs.edit();
		ed.putInt("newAC", newAC);
		ed.putInt("newMisc", newMisc);
		ed.putInt("newUSB", newUSB);
		ed.putInt("newLoadL", newLoadL);
		ed.putInt("newLoadH", newLoadH);
		ed.putBoolean("chkOnBoot", chkOnBoot.isChecked());
		ed.commit();
	}
	
	public void restoreSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS3", 0);
		if (prefs.getInt("newAC", 0) != 0 && prefs.getInt("newMisc", 0) != 0) {
			newAC = prefs.getInt("newAC", newAC);
			newMisc = prefs.getInt("newMisc", newMisc);
			newUSB = prefs.getInt("newUSB", newUSB);
			newLoadL = prefs.getInt("newLoadL", newLoadL);
			newLoadH = prefs.getInt("newLoadH", newLoadH);
			chkOnBoot.setChecked(prefs.getBoolean("chkOnBoot", chkOnBoot.isChecked()));
			applySettings();
		}
	}

}
