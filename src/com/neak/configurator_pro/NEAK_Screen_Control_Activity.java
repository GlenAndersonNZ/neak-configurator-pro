package com.neak.configurator_pro;


import java.io.File;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.neak.configurator_pro.toolset.fileTools;

public class NEAK_Screen_Control_Activity extends Activity {

	private boolean isEdit = false;
	
	private Button btnApply;
	private Button btnIndex;
	private CheckBox chkOnBoot;
	
	private EditText editMinGamma;
	private EditText editMaxGamma;
	private EditText editMinBL;
	
	private int newMinGamma, newMaxGamma, newMinBL;
	
	final fileTools tools = new fileTools();
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_screen_control_layout);
	
	    initiateUI();
	    checkSetOnBoot();
	}
		
	@Override
	public void onResume(){
		super.onResume();
		isEdit = false;
		initiateUI();
		checkSetOnBoot();
	}
	
	public void checkSetOnBoot() {
		File f;
		try{
			f = new File("/etc/init.d/S43neakScreen");
			if (f.exists()) {
				chkOnBoot.setChecked(true);
			}
			else {
				chkOnBoot.setChecked(false);
			}
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
	}
	
	public void initiateUI() {
		btnApply = (Button) findViewById(R.id.button_Screen_Apply);
		btnApply.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (isEdit) {
                	applySettings();
                	isEdit = false;
                	initiateUI();
                }
                else {
                	isEdit = true;
                	initiateUI();
                }
            }
        });
		btnIndex = (Button) findViewById(R.id.button_Screen_Control_Index);
		btnIndex.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
            }
        });
		chkOnBoot = (CheckBox) findViewById(R.id.check_Screen_OnBoot);
		
		editMinGamma = (EditText) findViewById(R.id.edit_Screen_Brightness_MinGamma);
		editMaxGamma = (EditText) findViewById(R.id.edit_Screen_Brightness_MaxGamma);
		editMinBL = (EditText) findViewById(R.id.edit_Screen_Brightness_minBL);
		
		loadValues();
		
		if (isEdit) {
			chkOnBoot.setEnabled(true);
			editMinGamma.setEnabled(true);
			editMaxGamma.setEnabled(true);
			editMinBL.setEnabled(true);
			btnApply.setText("Apply Settings");
		}
		else {
			chkOnBoot.setEnabled(false);
			editMinGamma.setEnabled(false);
			editMaxGamma.setEnabled(false);
			editMinBL.setEnabled(false);
			btnApply.setText("Edit Settings");
		}
	}
	
	public void loadValues() {
		newMinGamma = Integer.parseInt(tools.readSystemFile("/sys/devices/virtual/misc/brightness_curve/min_gamma"));
		editMinGamma.setText(newMinGamma + "");
		newMaxGamma = Integer.parseInt(tools.readSystemFile("/sys/devices/virtual/misc/brightness_curve/max_gamma"));
		editMaxGamma.setText(newMaxGamma + "");
		newMinBL = Integer.parseInt(tools.readSystemFile("/sys/devices/virtual/misc/brightness_curve/min_bl"));
		editMinBL.setText(newMinBL + "");
	}
		
	private void applySettings() {
		newMinGamma = Integer.parseInt(editMinGamma.getText().toString());
		newMaxGamma = Integer.parseInt(editMaxGamma.getText().toString());
		newMinBL = Integer.parseInt(editMinBL.getText().toString());
		String cmd;
		cmd = "echo \"" + newMinGamma + "\" > /sys/devices/virtual/misc/brightness_curve/min_gamma";
		tools.writeToFile(cmd);
		cmd = "echo \"" + newMaxGamma + "\" > /sys/devices/virtual/misc/brightness_curve/max_gamma";
		tools.writeToFile(cmd);
		cmd = "echo \"" + newMinBL + "\" > /sys/devices/virtual/misc/brightness_curve/min_bl";
		tools.writeToFile(cmd);
		if (chkOnBoot.isChecked()) {
			cmd = "#!/system/bin/sh\n"
				+ "echo \\\"" + newMinGamma + "\\\" > /sys/devices/virtual/misc/brightness_curve/min_gamma\n"
				+ "echo \\\"" + newMaxGamma + "\\\" > /sys/devices/virtual/misc/brightness_curve/max_gamma\n"
				+ "echo \\\"" + newMinBL + "\\\" > /sys/devices/virtual/misc/brightness_curve/min_bl\n";
			String cmd2 = "echo \"" + cmd + "\" > /etc/init.d/S43neakScreen";
			tools.writeToFile(cmd2);
		}
		else {
			cmd = "rm -f /etc/init.d/S43neakScreen";
			tools.writeToFile(cmd);
		}

	}
	
	@Override
	public void onBackPressed() {
		Toast.makeText(NEAK_Screen_Control_Activity.this, "Press back once more to exit", Toast.LENGTH_SHORT).show();
		NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
	}
	
	public void backupSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS5", 0);
		SharedPreferences.Editor ed = prefs.edit();
		ed.putInt("newMinGamma", newMinGamma);
		ed.putInt("newMaxGamma", newMaxGamma);
		ed.putInt("newMinBL", newMinBL);
		ed.putBoolean("chkOnBoot", chkOnBoot.isChecked());
		ed.commit();
	}
	
	public void restoreSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS5", 0);
		if (prefs.getInt("newMinGamma", 0) != 0 && prefs.getInt("newMaxGamma", 0) != 0) {
			newMinGamma = prefs.getInt("newMinGamma", newMinGamma);
			newMaxGamma = prefs.getInt("newMaxGamma", newMaxGamma);
			newMinBL = prefs.getInt("newMinBL", newMinBL);
			chkOnBoot.setChecked(prefs.getBoolean("chkOnBoot", chkOnBoot.isChecked()));
			applySettings();
		}
	}

}
