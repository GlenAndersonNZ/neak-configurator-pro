package com.neak.configurator_pro;

import java.io.File;

import com.neak.configurator_pro.toolset.fileTools;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class NEAK_GPU_Control_Activity extends Activity {
	
	private boolean isEdit = false;
	
	private Button btnIndex;
	
	private Button btnApply;
	private CheckBox chkOnBoot;
	
	private TextView txtFreqMax;
	private TextView txtFreqMin;
	private TextView txtVoltMaxTitle;
	private TextView txtVoltMax;
	private TextView txtVoltMinTitle;
	private TextView txtVoltMin;
	
	private SeekBar seekFreqMax;
	private SeekBar seekFreqMin;
	private SeekBar seekVoltMax;
	private SeekBar seekVoltMin;
	
	int newMaxFreq;
	int newMinFreq;
	int newMaxVolt;
	int newMinVolt;
	
	final fileTools tools = new fileTools();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_gpu_control_layout);
	    
	    initiateUI();
	    checkSetOnBoot();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		isEdit = false;
		initiateUI();
		checkSetOnBoot();
	}
	
	public void checkSetOnBoot() {
		File f;
		try {
			f = new File("/etc/init.d/S41neakGPU");
			if (f.exists()) {
				chkOnBoot.setChecked(true);
			}
			else {
				chkOnBoot.setChecked(false);
			}
		}
		catch (Exception e) {
			//Do Something...
		}
	}
	
	public void initiateUI(){
		btnIndex = (Button)findViewById(R.id.button_Backup_Index);
		btnIndex.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
            }
        });
		btnApply = (Button)findViewById(R.id.button_GPU_Apply);
		btnApply.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (isEdit) {
                	applySettings();
                	isEdit = false;
                	initiateUI();
                }
                else {
                	isEdit = true;
                	initiateUI();
                }
            }
        });
		chkOnBoot = (CheckBox)findViewById(R.id.check_GPU_ApplyOnBoot);
		txtFreqMax = (TextView)findViewById(R.id.text_GPU_FreqMax);
		txtFreqMin = (TextView)findViewById(R.id.text_GPU_FreqMin);
		txtVoltMaxTitle = (TextView)findViewById(R.id.text_GPU_VoltageMaxTitle);
		txtVoltMax = (TextView)findViewById(R.id.text_GPU_VoltageMax);
		txtVoltMinTitle = (TextView)findViewById(R.id.text_GPU_VoltageMinTitle);
		txtVoltMin = (TextView)findViewById(R.id.text_GPU_VoltageMin);
		
		seekFreqMax = (SeekBar)findViewById(R.id.seek_GPU_FreqMax);
		seekFreqMin = (SeekBar)findViewById(R.id.seek_GPU_FreqMin);
		seekVoltMax = (SeekBar)findViewById(R.id.seek_GPU_VoltageMax);
		seekVoltMin = (SeekBar)findViewById(R.id.seek_GPU_VoltageMin);
				
		String tmp = tools.readSystemFile("/sys/devices/virtual/misc/gpu_clock_control/gpu_control");
		
		switch(parseMaxFreqValue(tmp)) {
		case 100:
			txtFreqMax.setText("100 MHz");
			txtVoltMaxTitle.setText("100 MHz");
			seekFreqMax.setProgress(0);
			newMaxFreq = 100;
			break;
		case 133:
			txtFreqMax.setText("133 MHz");
			txtVoltMaxTitle.setText("133 MHz");
			seekFreqMax.setProgress(1);
			newMaxFreq = 133;
			break;
		case 160:
			txtFreqMax.setText("160 MHz");
			txtVoltMaxTitle.setText("160 MHz");
			seekFreqMax.setProgress(2);
			newMaxFreq = 160;
			break;
		case 267:
			txtFreqMax.setText("267 MHz");
			txtVoltMaxTitle.setText("267 MHz");
			seekFreqMax.setProgress(3);
			newMaxFreq = 267;
			break;
		case 400:
			txtFreqMax.setText("400 MHz");
			txtVoltMaxTitle.setText("400 MHz");
			seekFreqMax.setProgress(4);
			newMaxFreq = 400;
			break;
		default:
			txtFreqMax.setText("Unsupported - Changed to 267 MHZ");
			txtVoltMaxTitle.setText("267 MHz");
			seekFreqMax.setProgress(3);
			newMaxFreq = 267;
			break;
		}
		
		seekFreqMax.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					switch(progress) {
					case 0:
						txtFreqMax.setText("100 MHz");
						txtFreqMax.refreshDrawableState();
						txtVoltMaxTitle.setText("100 MHz");
						txtVoltMaxTitle.refreshDrawableState();
						newMaxFreq = 100;
						break;
					case 1:
						txtFreqMax.setText("133 MHz");
						txtFreqMax.refreshDrawableState();
						txtVoltMaxTitle.setText("133 MHz");
						txtVoltMaxTitle.refreshDrawableState();
						newMaxFreq = 133;
						break;
					case 2:
						txtFreqMax.setText("160 MHz");
						txtFreqMax.refreshDrawableState();
						txtVoltMaxTitle.setText("160 MHz");
						txtVoltMaxTitle.refreshDrawableState();
						newMaxFreq = 160;
						break;
					case 3:
						txtFreqMax.setText("267 MHz");
						txtFreqMax.refreshDrawableState();
						txtVoltMaxTitle.setText("267 MHz");
						txtVoltMaxTitle.refreshDrawableState();
						newMaxFreq = 267;
						break;
					case 4:
						txtFreqMax.setText("400 MHz");
						txtFreqMax.refreshDrawableState();
						txtVoltMaxTitle.setText("400 MHz");
						txtVoltMaxTitle.refreshDrawableState();
						newMaxFreq = 400;
						break;
					default:
						break;
					}
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		switch(parseMinFreqValue(tmp)) {
		case 100:
			txtFreqMin.setText("100 MHz");
			txtVoltMinTitle.setText("100 MHz");
			seekFreqMin.setProgress(0);
			newMinFreq = 100;
			break;
		case 133:
			txtFreqMin.setText("133 MHz");
			txtVoltMinTitle.setText("133 MHz");
			seekFreqMin.setProgress(1);
			newMinFreq = 133;
			break;
		case 160:
			txtFreqMin.setText("160 MHz");
			txtVoltMinTitle.setText("160 MHz");
			seekFreqMin.setProgress(2);
			newMinFreq = 160;
			break;
		case 267:
			txtFreqMin.setText("267 MHz");
			txtVoltMinTitle.setText("267 MHz");
			seekFreqMin.setProgress(3);
			newMinFreq = 267;
			break;
		case 400:
			txtFreqMin.setText("400 MHz");
			txtVoltMinTitle.setText("400 MHz");
			seekFreqMin.setProgress(4);
			newMinFreq = 400;
			break;
		default:
			txtFreqMin.setText("Unsupported - Changed to 100MHz");
			txtVoltMinTitle.setText("100MHz");
			seekFreqMin.setProgress(0);
			newMinFreq = 100;
			break;
		}
		
		seekFreqMin.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					switch(progress) {
					case 0:
						txtFreqMin.setText("100 MHz");
						txtFreqMin.refreshDrawableState();
						txtVoltMinTitle.setText("100 MHz");
						txtVoltMinTitle.refreshDrawableState();
						newMinFreq = 100;
						break;
					case 1:
						txtFreqMin.setText("133 MHz");
						txtFreqMin.refreshDrawableState();
						txtVoltMinTitle.setText("133 MHz");
						txtVoltMinTitle.refreshDrawableState();
						newMinFreq = 133;
						break;
					case 2:
						txtFreqMin.setText("160 MHz");
						txtFreqMin.refreshDrawableState();
						txtVoltMinTitle.setText("160 MHz");
						txtVoltMinTitle.refreshDrawableState();
						newMinFreq = 160;
						break;	
					case 3:
						txtFreqMin.setText("267 MHz");
						txtFreqMin.refreshDrawableState();
						txtVoltMinTitle.setText("267 MHz");
						txtVoltMinTitle.refreshDrawableState();
						newMinFreq = 267;
						break;
					case 4:
						txtFreqMin.setText("400 MHz");
						txtFreqMin.refreshDrawableState();
						txtVoltMinTitle.setText("400 MHz");
						txtVoltMinTitle.refreshDrawableState();
						newMinFreq = 400;
						break;
					default:
						break;
					}
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		tmp = tools.readSystemFile("/sys/devices/virtual/misc/gpu_voltage_control/gpu_control");
		int tmp1 = parseMaxVoltValue(tmp) / 1000;
		txtVoltMax.setText("Unsupported Voltage - set to 1000 mV");
		seekVoltMax.setProgress(6);
		newMaxVolt = 1000000;
		for (int i=0; i < 13; i++) {
			int test = i * 50 + 600;
			if (test == tmp1) {
				txtVoltMax.setText(tmp1 + " mV");
				seekVoltMax.setProgress(i);
				newMaxVolt = tmp1 * 1000;
			}
		}
		
		seekVoltMax.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					int newVal = seekVoltMax.getProgress() * 50 + 600;
					txtVoltMax.setText(newVal + " mV");
					newMaxVolt = newVal * 1000;
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		tmp1 = parseMinVoltValue(tmp) / 1000;
		txtVoltMin.setText("Unsupported Voltage - set to 900 mV");
		seekVoltMin.setProgress(4);
		newMinVolt = 900000;
		for (int i=0; i < 13; i++) {
			int test = i * 50 + 600;
			if (test == tmp1) {
				txtVoltMin.setText(tmp1 + " mV");
				seekVoltMin.setProgress(i);
				newMinVolt = tmp1 * 1000;
			}
		}
		
		seekVoltMin.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					int newVal = seekVoltMin.getProgress() * 50 + 600;
					txtVoltMin.setText(newVal + " mV");
					newMinVolt = newVal * 1000;
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		
		if (isEdit) {
			btnApply.setText("Apply Settings");
			chkOnBoot.setEnabled(true);
			seekFreqMax.setEnabled(true);
			seekFreqMin.setEnabled(true);
			seekVoltMax.setEnabled(true);
			seekVoltMin.setEnabled(true);
		}
		else {
			btnApply.setText("Edit Settings");
			chkOnBoot.setEnabled(false);
			seekFreqMax.setEnabled(false);
			seekFreqMin.setEnabled(false);
			seekVoltMax.setEnabled(false);
			seekVoltMin.setEnabled(false);
		}
		
	}
	
	private int parseMaxFreqValue(String toParse) {
		try {
			String parsedString = toParse.substring(toParse.indexOf("1: ") + 3);
			return Integer.parseInt(parsedString);
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return 0;
	}
	
	private int parseMinFreqValue(String toParse) {
		try {
			String parsedString = toParse.substring(toParse.indexOf("0: ") + 3, toParse.indexOf("Step1"));
			return Integer.parseInt(parsedString);
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return 0;
	}
	
	private int parseMaxVoltValue(String toParse) {
		try {
			String parsedString = toParse.substring(toParse.indexOf("2: ") + 3);
			return Integer.parseInt(parsedString);
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return 0;
	}
	
	private int parseMinVoltValue(String toParse) {
		try {
			String parsedString = toParse.substring(toParse.indexOf("1: ") + 3, toParse.indexOf("Step2"));
			return Integer.parseInt(parsedString);
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return 0;
	}
	
	public void applySettings() {
		String cmd;
		cmd = "echo " + newMinFreq + " " + newMaxFreq + " > /sys/devices/virtual/misc/gpu_clock_control/gpu_control";
		tools.writeToFile(cmd);
		cmd = "echo " + newMinVolt + " " + newMaxVolt + " > /sys/devices/virtual/misc/gpu_voltage_control/gpu_control";
		tools.writeToFile(cmd);		
		if (chkOnBoot.isChecked()) {
			String data = "#!/system/bin/sh\n"
					+ "echo \\\"" + newMinFreq + " " + newMaxFreq + "\\\" > /sys/devices/virtual/misc/gpu_clock_control/gpu_control\n"
					+ "echo \\\"" + newMinVolt + " " + newMaxVolt + "\\\" > /sys/devices/virtual/misc/gpu_voltage_control/gpu_control\n"; 
			cmd = "echo \"" + data + "\" > /etc/init.d/S41neakGPU";
			tools.writeToFile(cmd);
		}
		else {
			cmd = "rm -f /etc/init.d/S41neakGPU";
			tools.writeToFile(cmd);
		}
		cmd = null;
	}
	
	@Override
	public void onBackPressed() {
		Toast.makeText(NEAK_GPU_Control_Activity.this, "Press back once more to exit", Toast.LENGTH_SHORT).show();
		NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
	}
	
	public void backupSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS1", 0);
		SharedPreferences.Editor ed = prefs.edit();
		ed.putInt("newMaxFreq", newMaxFreq);
		ed.putInt("newMinFreq", newMinFreq);
		ed.putInt("newMaxVolt", newMaxVolt);
		ed.putInt("newMinVolt", newMinVolt);
		ed.putBoolean("chkOnBoot", chkOnBoot.isChecked());
		ed.commit();
	}
	
	public void restoreSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS1", 0);
		if (prefs.getInt("newMaxFreq", 0) != 0 && prefs.getInt("newMinFreq", 0) != 0) {
			newMaxFreq = prefs.getInt("newMaxFreq", newMaxFreq);
			newMinFreq = prefs.getInt("newMinFreq", newMinFreq);
			newMaxVolt = prefs.getInt("newMaxVolt", newMaxVolt);
			newMinVolt = prefs.getInt("newMinVolt", newMinVolt);
			chkOnBoot.setChecked(prefs.getBoolean("chkOnBoot", chkOnBoot.isChecked()));
			applySettings();
		}
	}


}
