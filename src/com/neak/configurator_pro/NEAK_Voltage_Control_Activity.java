package com.neak.configurator_pro;

import java.io.File;
import java.util.ArrayList;

import com.neak.configurator_pro.toolset.fileTools;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class NEAK_Voltage_Control_Activity extends Activity {

	/** Called when the activity is first created. */
	
	private boolean isEdit = false;
	
	ArrayList<TextView> textViewArray = new ArrayList<TextView>();
	ArrayList<String> frequencyArray = new ArrayList<String>();
	ArrayList<Integer> voltageArray = new ArrayList<Integer>();
	ArrayList<SeekBar> sliderArray = new ArrayList<SeekBar>();
	ArrayList<RelativeLayout> layoutArray = new ArrayList<RelativeLayout>();
	
	private Button btnIndex;
	private Button btnApply;
	
	private CheckBox chkOnBoot;
	
		
	final fileTools tools = new fileTools();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_voltage_control_layout);
	    
	    initiateUI();
	    checkSetOnBoot();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		isEdit = false;
		refreshUI();
		checkSetOnBoot();
	}
	
	public void checkSetOnBoot() {
		File f;
		try {
			f = new File("/etc/init.d/S42neakVoltage");
			if (f.exists()) {
				chkOnBoot.setChecked(true);
			}
			else {
				chkOnBoot.setChecked(false);
			}
		}
		catch (Exception e) {
			//Do Something...
		}
	}
	
	public void initiateUI(){
		btnApply = (Button) findViewById(R.id.button_Volt_Apply);
		btnApply.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (isEdit) {
                	applySettings();
                	isEdit = false;
                	refreshUI();
                }
                else {
                	isEdit = true;
                	refreshUI();
                }
            }
        });
		btnIndex = (Button) findViewById(R.id.button_Voltage_Index);
		btnIndex.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
            }
        });
		chkOnBoot = (CheckBox) findViewById(R.id.check_Volt_ApplyOnBoot);
		
		
		ArrayList<String> tmp = tools.readSystemFileArray("/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table");
		
		frequencyArray.clear();
		String tmpString = "";
		for (int i = 0; i < tmp.size(); i++) {
			if (!(tmp.get(i).toString().equals(null))){
				tmpString = parseFrequencyValue(tmp.get(i).toString(), ":");
				frequencyArray.add(tmpString);
			}
		}
				
		voltageArray.clear();
		int tmpValue = 0;
		for (int i = 0; i < tmp.size(); i++) {
			if (!(tmp.get(i).toString().equals(null))){
				tmpValue = parseVoltageValue(tmp.get(i).toString(), ":", 2);
				voltageArray.add(tmpValue);
			}
		}
		
		LinearLayout mainLayout = (LinearLayout)findViewById(R.id.Voltage_Layout);
		textViewArray.clear();
		sliderArray.clear();
		layoutArray.clear();
		
		for (int i = 0; i < frequencyArray.size(); i++) {
			RelativeLayout rl = (RelativeLayout)View.inflate(this, R.layout.neak_seekbar_layout, null);
			mainLayout.addView(rl);
			layoutArray.add(rl);
			TextView tvTitle = (TextView) rl.findViewById(R.id.vc_title);
			final TextView tvValue = (TextView) rl.findViewById(R.id.vc_amount);
			SeekBar sb = (SeekBar) rl.findViewById(R.id.vc_seekbar);
			
			tvTitle.setText(frequencyArray.get(i).toString());
						
			tvValue.setText(voltageArray.get(i).toString());
			textViewArray.add(tvValue);
					
			sb.setMax(28);
			int itmp = (voltageArray.get(i) - 700) / 25;
			sb.setProgress(itmp);
			sliderArray.add(sb);
			
			final int currentIndex = i;
			sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					if (fromUser) {
						int newVal = seekBar.getProgress() * 25 + 700;
						tvValue.setText("" + newVal);
						voltageArray.set(currentIndex, newVal);
					}
	            }

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
				}
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					// TODO Auto-generated method stub
				}
			});
			
			if (!isEdit) {
				sb.setEnabled(false);
			}
	
		}
		
		if (isEdit) {
			chkOnBoot.setEnabled(true);
			btnApply.setText("Apply Settings");
		}
		else {
			chkOnBoot.setEnabled(false);
			btnApply.setText("Edit Settings");
		}
		
	}
	
	private void refreshUI() {
		ArrayList<String> tmp = tools.readSystemFileArray("/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table");
		
		voltageArray.clear();
		int tmpValue = 0;
		for (int i = 0; i < tmp.size(); i++) {
			if (!(tmp.get(i).toString().equals(null))){
				tmpValue = parseVoltageValue(tmp.get(i).toString(), ":", 2);
				voltageArray.add(tmpValue);
			}
		}
		
		for (int i = 0; i < frequencyArray.size(); i++) {
			textViewArray.get(i).setText(voltageArray.get(i).toString());
			int itmp = (voltageArray.get(i) - 700) / 25;
			sliderArray.get(i).setProgress(itmp);
			if (isEdit) {
				sliderArray.get(i).setEnabled(true);
			}
			else {
				sliderArray.get(i).setEnabled(false);
			}
		}
		if (isEdit) {
			chkOnBoot.setEnabled(true);
			btnApply.setText("Apply Settings");
		}
		else {
			chkOnBoot.setEnabled(false);
			btnApply.setText("Edit Settings");
		}
		
	}
	
	private int parseVoltageValue(String toParse, String start, int mod) {
		try {
			String parsedString = toParse.substring(toParse.indexOf(start) + mod, toParse.indexOf("mV") -1);
			return Integer.parseInt(parsedString);
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return 0;
	}
	
	private String parseFrequencyValue(String toParse, String stop) {
		try {
			String parsedString = toParse.substring(0, toParse.indexOf(stop));
			return parsedString;
		}
		catch (NullPointerException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return "";
	}

	public void applySettings() {
		String cmd;
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append(voltageArray.get(0));
		for (int i = 1; i < frequencyArray.size(); i++) {
			sBuilder.append(" ");
			sBuilder.append(voltageArray.get(i));
		}
		String values = sBuilder.toString();				
		cmd = "echo \"" + values + "\" > /sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table";
		tools.writeToFile(cmd);
		if (chkOnBoot.isChecked()) {
		cmd = "#!/system/bin/sh\n"
			+ "echo \\\"" + values + "\\\" > /sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table\n";
		String cmd2 = "echo \"" + cmd + "\" > /etc/init.d/S42neakVoltage";
		tools.writeToFile(cmd2);
		}
		else {
			cmd = "rm -f /etc/init.d/S42neakVoltage";
			tools.writeToFile(cmd);
		}
	}

	@Override
	public void onBackPressed() {
		Toast.makeText(NEAK_Voltage_Control_Activity.this, "Press back once more to exit", Toast.LENGTH_SHORT).show();
		NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
	}
	
	public void backupSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS5", 0);
		SharedPreferences.Editor ed = prefs.edit();
		String tmp;
		int tmp1;
		String PREFIX = "step";
		initiateUI();
		for (int i=0; i < voltageArray.size(); i++) {
			tmp = PREFIX + i;
			tmp1 = voltageArray.get(i);
			ed.putInt(tmp, tmp1);
		}
		ed.putBoolean("chkOnBoot", chkOnBoot.isChecked());
		ed.commit();
	}
	
	public void restoreSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS5", 0);
		if (prefs.getInt("step0", 0) != 0 && prefs.getInt("step1", 0) != 0) {
			String tmp;
			String PREFIX = "step";
			initiateUI();
			for (int i=0; i < voltageArray.size(); i++) {
				tmp = PREFIX + i;
				voltageArray.set(i, prefs.getInt(tmp, voltageArray.get(i)));
			}
			chkOnBoot.setChecked(prefs.getBoolean("chkOnBoot", chkOnBoot.isChecked()));
			applySettings();
		}
	}

}
