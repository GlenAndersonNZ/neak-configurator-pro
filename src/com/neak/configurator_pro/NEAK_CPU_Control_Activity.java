package com.neak.configurator_pro;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.neak.configurator_pro.toolset.fileTools;

public class NEAK_CPU_Control_Activity extends Activity {

	/** Called when the activity is first created. */

	private boolean isEdit = false;

	private Button btnIndex;
	private TextView txtCPUCurrentFreq;
	private Handler mHandler = new Handler();
	private Runnable mUpdateTimeTask = new Runnable() {
		   public void run() {
			   displayCurrentFreq();
		       mHandler.postDelayed(this, 500);
		   }
		};
	private SeekBar seekMax;
	private SeekBar seekMin;
	private TextView maxCPU;
	private TextView minCPU;

	private int newMaxFreq;
	private int newMinFreq;
	private String newGovernor;
	private String newScheduler;

	private Spinner spinGovernor;
	private Spinner spinScheduler;

	private CheckBox setOnBoot;
	private Button applySettings;

	ArrayAdapter<String> adapterGovernor;
	ArrayAdapter<String> adapterScheduler;
	
	private int[] freqs;
	
	final fileTools tools = new fileTools();

	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_cpu_control_layout);

	    initiateUI();
	    checkSetOnBoot();
	    txtCPUCurrentFreq.setText("Initiating CPU");

	}

	@Override
	public void onResume() {
		super.onResume();
		try {
			mHandler.removeCallbacks(mUpdateTimeTask);
            mHandler.postDelayed(mUpdateTimeTask, 100);
            checkSetOnBoot();
		} catch (Exception e) { /*do nothing*/ }	
		isEdit = false;
		initiateUI();
		checkSetOnBoot();
	}

	@Override
	public void onPause() {
		super.onPause();
		try {
			mHandler.removeCallbacks(mUpdateTimeTask);
		} catch (Exception e) { /*do nothing*/ }
	}

	public void checkSetOnBoot() {
		File f;
		try {
			f = new File("/etc/init.d/S40neakCPU");
			if (f.exists()) {
				setOnBoot.setChecked(true);
			}
			else {
				setOnBoot.setChecked(false);
			}
		}
		catch (Exception e) {
			//Do Something...
		}
	}
	
	public void initiateUI() {
		btnIndex = (Button)findViewById(R.id.button_CPU_Index);
		btnIndex.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
            }
        });
		txtCPUCurrentFreq = (TextView) findViewById(R.id.text_freq_current);
		maxCPU = (TextView) findViewById(R.id.text_freq_max_display);
		minCPU = (TextView) findViewById(R.id.text_freq_min_display);

		int tmp1 = tools.getCurrentFrequency("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
		tmp1 = tmp1 / 1000;
		maxCPU.setText(tmp1 + " MHz");

		File f = new File("/sys/devices/system/cpu/cpu0/cpufreq/freq_table");
		if (f.exists()) {
			String readFreq = tools.readSystemFile("/sys/devices/system/cpu/cpu0/cpufreq/freq_table");
			String[] stmp = readFreq.split(" ");
			int tmpfreqs[] = new int[stmp.length];
			int j = 0;
			for (int i = stmp.length - 1; i >= 0 ; i--) {
				tmpfreqs[j] = Integer.parseInt(stmp[i]);
				j++;
			}
			freqs = tmpfreqs;
		}
		else {
			String readFreq = tools.readSystemFile("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies");
			String[] stmp = readFreq.split(" ");
			int tmpfreqs[] = new int[stmp.length];
			int j = 0;
			for (int i = stmp.length - 1; i >= 0 ; i--) {
				tmpfreqs[j] = (Integer.parseInt(stmp[i])/1000);
				j++;
			}
			freqs = tmpfreqs;
		}
				
		int tmp2 = tools.getCurrentFrequency("/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
		tmp2 = tmp2 / 1000;
		minCPU.setText(tmp2 + " MHz");

		seekMax = (SeekBar) findViewById(R.id.seekBar_max_freq);
		seekMax.setMax((freqs.length - 1));
		for (int i = 0; i < freqs.length; i++) {
			if (freqs[i] == tmp1) {
				seekMax.setProgress(i);
			}
		}
		seekMax.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					maxCPU.setText(freqs[progress] + " Mhz");
					maxCPU.refreshDrawableState();
					newMaxFreq = freqs[progress] * 1000;
				}
            }

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});
		seekMin = (SeekBar) findViewById(R.id.seekBar_min_freq);
		seekMin.setMax((freqs.length - 1));
		for (int i = 0; i < freqs.length; i++) {
			if (freqs[i] == tmp2) {
				seekMin.setProgress(i);
			}
		}
		seekMin.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser) {
					minCPU.setText(freqs[progress] + " MHz");
					minCPU.refreshDrawableState();
					newMinFreq = freqs[progress] * 1000;
				}
            }
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
			}
		});

		spinGovernor = (Spinner) findViewById(R.id.spinner_governor);
		ArrayList<String> governorArray = getStringArray(tools.readSystemFile("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors"));
		adapterGovernor = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, governorArray);
		adapterGovernor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinGovernor.setAdapter(adapterGovernor);
		spinGovernor.setSelection(adapterGovernor.getPosition(tools.readSystemFile("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor")));

		spinScheduler = (Spinner) findViewById(R.id.spinner_scheduler);
		ArrayList<String> schedulerArray = getStringArray(tools.readSystemFile("/sys/block/mmcblk0/queue/scheduler"));
		adapterScheduler = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, schedulerArray);
		adapterScheduler.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinScheduler.setAdapter(adapterScheduler);
		spinScheduler.setSelection(adapterScheduler.getPosition(getCurrentSched(tools.readSystemFile("/sys/block/mmcblk0/queue/scheduler"))));

		setOnBoot = (CheckBox) findViewById(R.id.check_CPU_onBoot);

		applySettings = (Button) findViewById(R.id.button_CPU_Apply);
		applySettings.setOnClickListener(new OnClickListener(){
            public void onClick(View v) {
            	if (isEdit) {
            		applySettings();
            		isEdit = false;
            		initiateUI();            		
            	}
            	else {
            		isEdit = true;
            		initiateUI();
            	}
            }
        });
		
		if (isEdit) {
			seekMax.setEnabled(true);
			seekMin.setEnabled(true);
			spinGovernor.setEnabled(true);
			spinScheduler.setEnabled(true);
			setOnBoot.setEnabled(true);
			applySettings.setText("Apply Settings");
		}
		else {
			seekMax.setEnabled(false);
			seekMin.setEnabled(false);
			spinGovernor.setEnabled(false);
			spinScheduler.setEnabled(false);
			setOnBoot.setEnabled(false);
			applySettings.setText("Edit Settings");
		}

	}

	public void displayCurrentFreq() {
		String result = "Unable to Obtain";
		txtCPUCurrentFreq.setText(result);
		int iCPUFreq;
		
		iCPUFreq = tools.getCurrentFrequency("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq");
		iCPUFreq = iCPUFreq / 1000;
		result = iCPUFreq + " MHz";

		txtCPUCurrentFreq.setText(result);
		txtCPUCurrentFreq.refreshDrawableState();
	}

	public ArrayList<String> getStringArray(String toParse) {
		ArrayList<String> arrayString = new ArrayList<String>();
		Scanner s = new Scanner(toParse).useDelimiter("\\W+");
		while(s.hasNext()) {
			arrayString.add(s.next());
		}
		return arrayString;
	}

	private String getCurrentSched(String toParse) {
		String activeSched = toParse.substring(toParse.indexOf('[') + 1, toParse.indexOf(']'));
		return activeSched;
	}

	private void applySettings() {
		
		newGovernor = adapterGovernor.getItem(spinGovernor.getSelectedItemPosition());
		newScheduler = adapterScheduler.getItem(spinScheduler.getSelectedItemPosition());

		if (setOnBoot.isChecked()) {
			writeMaxFreq(newMaxFreq);
			writeMinFreq(newMinFreq);
			writeGovernor(newGovernor);
			writeScheduler(newScheduler);
			applyOnBoot(newMaxFreq, newMinFreq, newGovernor, newScheduler);
		}
		else {
			writeMaxFreq(newMaxFreq);
			writeMinFreq(newMinFreq);
			writeGovernor(newGovernor);
			writeScheduler(newScheduler);
			String cmd = "rm -f /etc/init.d/S40neakCPU";
			tools.writeToFile(cmd);
		}
	}

	private void writeMaxFreq(int selected) {
		String cmd = "echo " + selected + " > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq";
		tools.writeToFile(cmd);
	}

	private void writeMinFreq(int selected) {
		String cmd = "echo " + selected + " > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq";
		tools.writeToFile(cmd);
	}

	private void writeGovernor(String selected) {
		String cmd = "echo " + selected + " > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor";
		tools.writeToFile(cmd);
	}

	private void writeScheduler(String selected) {
		String cmd = "echo " + selected + " > /sys/block/mmcblk0/queue/scheduler";
		tools.writeToFile(cmd);
		cmd = "echo " + selected + " > /sys/block/mmcblk1/queue/scheduler";
		tools.writeToFile(cmd);
	}

	private void applyOnBoot(int max, int min, String governor, String scheduler) {
		String data = "#!/system/bin/sh\n"
				+ "sleep 20\n"
				+ "echo \\\"" + min + "\\\" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq\n"
				+ "echo \\\"" + max + "\\\" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq\n"
				+ "echo \\\"" + governor + "\\\" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor\n"
				+ "echo \\\"" + scheduler + "\\\" > /sys/block/mmcblk0/queue/scheduler\n"
				+ "echo \\\"" + scheduler + "\\\" > /sys/block/mmcblk1/queue/scheduler\n"; 
		String cmd = "echo \"" + data + "\" > /etc/init.d/S40neakCPU";
		tools.writeToFile(cmd);
	}

	@Override
	public void onBackPressed() {
		Toast.makeText(NEAK_CPU_Control_Activity.this, "Press back once more to exit", Toast.LENGTH_SHORT).show();
		NEAK_Configurator_Activity.tabHost.setCurrentTab(0);
	}
	
	public void backupSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS", 0);
		SharedPreferences.Editor ed = prefs.edit();
		ed.putInt("newMaxFreq", newMaxFreq);
		ed.putInt("newMinFreq", newMinFreq);
		ed.putString("newGovernor", newGovernor);
		ed.putString("newScheduler", newScheduler);
		ed.putBoolean("setOnBoot", setOnBoot.isChecked());
		ed.commit();
	}
	
	public void restoreSettings() {
		SharedPreferences prefs = getSharedPreferences("PREFS", 0);
		if (prefs.getInt("newMaxFreq", 0) != 0 && prefs.getInt("newMinFreq", 0) != 0) {
			newMaxFreq = prefs.getInt("newMaxFreq", newMaxFreq);
			newMinFreq = prefs.getInt("newMinFreq", newMinFreq);
			newGovernor = prefs.getString("newGovernor", newGovernor);
			newScheduler = prefs.getString("newScheduler", newScheduler);
			setOnBoot.setChecked(prefs.getBoolean("setOnBoot", setOnBoot.isChecked()));
			applySettings();
		}
	}

}
