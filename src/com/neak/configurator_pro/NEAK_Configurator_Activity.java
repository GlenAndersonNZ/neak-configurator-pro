package com.neak.configurator_pro;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Dialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;

public class NEAK_Configurator_Activity extends TabActivity {
    /** Called when the activity is first created. */
    
    private TextView aboutKernelVersion;
    public static TabHost tabHost;
    public static TabWidget tabWidget;
    public static boolean tabsVisible = true; 
	
	@Override
    public void onCreate(Bundle savedInstanceState) throws NullPointerException {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
        
        tabHost = getTabHost();
        tabWidget = getTabWidget();
        tabWidget.setVisibility(TabWidget.VISIBLE); 
        
        // Tab for Index page
        TabSpec indexspec = tabHost.newTabSpec("Index");
        indexspec.setIndicator("  Index  ");
        Intent indexIntent = new Intent(this, NEAK_Index_Activity.class);
        indexspec.setContent(indexIntent);
        
        // Tab for NEAK Options
        TabSpec neakspec = tabHost.newTabSpec("NEAK Options");
        neakspec.setIndicator("  NEAK Options  ");
        Intent neakIntent = new Intent(this, NEAK_Options_Activity.class);
        neakspec.setContent(neakIntent);
 
        // Tab for OTA Downloader
        TabSpec otaspec = tabHost.newTabSpec("OTA Downloader");
        otaspec.setIndicator("  OTA Downloader  ");
        Intent otaIntent = new Intent(this, OTA_Downloader_Activity.class);
        otaspec.setContent(otaIntent);
        
        // Tab for CPU Control
        TabSpec cpuspec = tabHost.newTabSpec("CPU Control");
        cpuspec.setIndicator("  CPU Control  ");
        Intent cpuIntent = new Intent(this, NEAK_CPU_Control_Activity.class);
        cpuspec.setContent(cpuIntent);
        
        // Tab for GPU Control
        TabSpec gpuspec = tabHost.newTabSpec("GPU Control");
        gpuspec.setIndicator("  GPU Control  ");
        Intent gpuIntent = new Intent(this, NEAK_GPU_Control_Activity.class);
        gpuspec.setContent(gpuIntent);
        
        // Tab for S3 GPU Control
        TabSpec gpuspecS3 = tabHost.newTabSpec("S3 GPU Control");
        gpuspecS3.setIndicator("  GPU Control  ");
        Intent gpuS3Intent = new Intent(this, NEAK_GPU_S3_Control_Activity.class);
        gpuspecS3.setContent(gpuS3Intent);
        
        // Tab for Voltage Control
        TabSpec voltagespec = tabHost.newTabSpec("Voltage Control");
        voltagespec.setIndicator("  Voltage Control  ");
        Intent voltageIntent = new Intent(this, NEAK_Voltage_Control_Activity.class);
        voltagespec.setContent(voltageIntent);
        
        // Tab for Internal Voltage Control
        TabSpec intvoltspec = tabHost.newTabSpec("Internal Voltages");
        intvoltspec.setIndicator("  Internal Voltages  ");
        Intent intvoltIntent = new Intent(this, NEAK_Internal_Voltages_Activity.class);
        intvoltspec.setContent(intvoltIntent);
        
        // Tab for Screen Options
        TabSpec screenspec = tabHost.newTabSpec("Screen Control");
        screenspec.setIndicator("  Screen Control  ");
        Intent screenIntent = new Intent(this, NEAK_Screen_Control_Activity.class);
        screenspec.setContent(screenIntent);
        
        // Tab for Misc Options
        TabSpec miscspec = tabHost.newTabSpec("Misc Options");
        miscspec.setIndicator("  Misc Options  ");
        Intent miscIntent = new Intent(this, NEAK_Misc_Options_Activity.class);
        miscspec.setContent(miscIntent);
        
     // Tab for Misc Options S3
        TabSpec miscspecS3 = tabHost.newTabSpec("Misc Options S3");
        miscspecS3.setIndicator("  Misc Options  ");
        Intent miscIntentS3 = new Intent(this, NEAK_Misc_Options_S3_Activity.class);
        miscspecS3.setContent(miscIntentS3);
         
        // Adding all TabSpec to TabHost
        File neakTest = new File("data/neak/");
        
        if (neakTest.isDirectory()) {
        	if (getSystemProperty("ro.product.model").equalsIgnoreCase("GT-I9300")) {
            	NEAK_Index_Activity.setNeakKernel(true);
            	NEAK_Index_Activity.setSThree(true);
            	tabHost.addTab(indexspec);
    	        tabHost.addTab(neakspec); 
    	        //tabHost.addTab(otaspec); 
    	        tabHost.addTab(cpuspec);
    	        tabHost.addTab(gpuspecS3);
    	        tabHost.addTab(voltagespec);
    	        tabHost.addTab(intvoltspec);
    	        tabHost.addTab(screenspec);
    	        tabHost.addTab(miscspecS3);
            }
        	else {
	        	NEAK_Index_Activity.setNeakKernel(true);
	        	tabHost.addTab(indexspec);
		        tabHost.addTab(neakspec); 
		        tabHost.addTab(otaspec); 
		        tabHost.addTab(cpuspec);
		        tabHost.addTab(gpuspec);
		        tabHost.addTab(voltagespec);
		        tabHost.addTab(screenspec);
		        tabHost.addTab(miscspec);
        	}
        }
        else {
        	NEAK_Index_Activity.setNeakKernel(false);
        	tabHost.addTab(indexspec);
	        tabHost.addTab(cpuspec);
	        tabHost.addTab(voltagespec);
	        tabHost.addTab(miscspec);
        }
        
        // Set correct permissions for files
        try {
			File f = new File("system/etc/init.d/");
        	Process p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/block/mmcblk0/queue/scheduler\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/block/mmcblk1/queue/scheduler\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/virtual/misc/gpu_clock_control/gpu_control\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/virtual/misc/gpu_voltage_control/gpu_control\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/system/cpu/cpu0/cpufreq/int_mV_table\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/virtual/misc/second_core/hotplug_on\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/vurtual/misc/second_core/second_core_on\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/virtual/misc/charge_current/charge_current\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/virtual/misc/brightness_curve/max_gamma\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/virtual/misc/brightness_curve/min_gamma\n");
			os.flush();
			os.writeBytes("chmod 755 /sys/devices/virtual/misc/brightness_curve/min_bl\n");
			os.flush();
			if (!f.isDirectory()) {
				os.writeBytes("mkdir /etc/init.d\n");
				os.flush();
				f.mkdirs();
			}
			os.writeBytes("chmod 755 system/etc/init.d\n");
			os.flush();
			os.writeBytes("mount -o remount ro /system\n");
			os.flush();
			
		}
        catch (IOException e) {
        	
        }
        
        
    }
	
	
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        if (tabWidget.getVisibility() == TabWidget.GONE) {
        	tabsVisible = false;
        }
        else {
        	tabsVisible = true;
        }
        
        return true;
    }
    
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.menu_showtabs);
        if (tabsVisible) {
        	item.setTitle("Hide Tabs");
        }
        else {
        	item.setTitle("Show Tabs");
        }
        
        return super.onPrepareOptionsMenu(menu);
    }

    
    /**
     * Menu option click handler
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
 
        switch (item.getItemId())
        {
        case R.id.menu_about:
            // Display about dialog
            final Dialog aboutDialog = new Dialog(NEAK_Configurator_Activity.this);
            aboutDialog.setContentView(R.layout.about_dialog);
            aboutDialog.setTitle("NEAK Configurator Pro App");
            aboutDialog.setCancelable(true);
            
            try {
            	aboutKernelVersion = (TextView)aboutDialog.findViewById(R.id.textview_aboutKernelVersion);
            	if (getSystemProperty("ro.neak.version").equalsIgnoreCase("Not a NEAK Kernel")) {
            		aboutKernelVersion.setText("Kernel Version: " + getSystemProperty("ro.kernel.id"));
            	}
            	else {
            		aboutKernelVersion.setText("Kernel Version: " + getSystemProperty("ro.neak.version"));
            	}
            }
            catch (Exception e){
            	e.printStackTrace();
            }
            
            Button about_button = (Button) aboutDialog.findViewById(R.id.button_aboutClose);
            about_button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                    aboutDialog.cancel();
                }
            });
            aboutDialog.show();
            return true;
 
        case R.id.menu_exit:
            // Close the application
        	NEAK_Configurator_Activity.this.finish();
            return true;
 
        case R.id.menu_test:
            String url = "market://details?id=com.neak.tester";
            Intent urlintent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
            startActivity(urlintent);
            return true;
 
        case R.id.menu_showtabs:
        	if (tabsVisible) {
        		tabWidget.setVisibility(TabWidget.GONE);
        		tabWidget.invalidate();
        		tabsVisible = false;
        	}
        	else {
        		tabWidget.setVisibility(TabWidget.VISIBLE);
        		tabsVisible = true;
        	}
        	return true;
        
        default:
            return super.onOptionsItemSelected(item);
        }
    } 
    
    /**
     * Returns a SystemProperty
     *
     * @param propName The Property to retrieve
     * @return The Property, or NULL if not found
     */
    public String getSystemProperty(String propName) {
    	String line;
        BufferedReader input = null;
        try {
            Process p = Runtime.getRuntime().exec("getprop " + propName);
            input = new BufferedReader(new InputStreamReader(p.getInputStream()), 1024);
            line = input.readLine();
            input.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        finally {
        	if (input != null) {
                try {
                    input.close();
                }
                catch (Exception e) {
                	e.printStackTrace();
                }
            }
        }

        if (input != null && line != null) {
        	if (line.equalsIgnoreCase("")) {
        		return "Not a NEAK Kernel";
        	}
        	else {
        		return line;
        	}
        }
        else {
        	return "Not a NEAK Kernel";
        }
    }
    
    public static class FlingableTabHost extends TabHost {
        GestureDetector mGestureDetector;

        Animation mRightInAnimation;
        Animation mRightOutAnimation;
        Animation mLeftInAnimation;
        Animation mLeftOutAnimation;

        public FlingableTabHost(Context context, AttributeSet attrs) {
            super(context, attrs);

            mRightInAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_right_in);
            mRightOutAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_right_out);
            mLeftInAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_left_in);
            mLeftOutAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_left_out);

            final int minScaledFlingVelocity = ViewConfiguration.get(context)
                    .getScaledMinimumFlingVelocity() * 20; // 10 = fudge by experimentation

            mGestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
				@Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                        float velocityY) {
                    int tabCount = getTabWidget().getTabCount();
                    int currentTab = getCurrentTab();
                    
                    
                    
                    if (Math.abs(velocityX) > minScaledFlingVelocity &&
                        Math.abs(velocityY) < minScaledFlingVelocity) {

                    	boolean right = velocityX < 0;
                        int newTab = currentTab;
                        
                        if (velocityX < 0) {
                        	if ((currentTab + 1) > (tabCount - 1)) {
                        		newTab = 0;
                        	} else {
                        		newTab = currentTab + 1;
                        	}
                        }
                        if (velocityX > 0) {
                        	if ((currentTab - 1) < 0) {
                        		newTab = tabCount - 1;
                        	} else {
                        		newTab = currentTab - 1;
                        	}
                        }
                                                
                        if (newTab != currentTab) {
                            View currentView = getCurrentView();
                            setCurrentTab(newTab);
                            View newView = getCurrentView();

                           newView.startAnimation(right ? mRightInAnimation : mLeftInAnimation);
                            currentView.startAnimation(
                                    right ? mRightOutAnimation : mLeftOutAnimation);
                        }
                    }
                    //return super.onFling(e1, e2, velocityX, velocityY); doesn't consume touch event
                    return false;
                }
                
            });
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            if (this.mGestureDetector.onTouchEvent(ev)) {
                return true;
            }
            return super.onInterceptTouchEvent(ev);
        }
        
        @Override
        public boolean dispatchTouchEvent(MotionEvent ev) {
        	if (this.mGestureDetector.onTouchEvent(ev)) {
                return true;
            }
            return super.dispatchTouchEvent(ev);
        }
    }
    
}