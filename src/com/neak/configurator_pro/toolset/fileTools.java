package com.neak.configurator_pro.toolset;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import android.util.Log;

public class fileTools {

	public String readSystemFile(String file) {
		InputStream in = null;
		String contents;
		try {
			Process p = new ProcessBuilder(new String[] { "cat", file }).start();
			in = p.getInputStream();
			contents = readAll(in);
			p.waitFor();
			p.destroy();
			return contents;
		}
		catch (IOException e) {
			Log.e("EXCEPTION: ", e.toString());
		} 
		catch (InterruptedException e) {
			Log.e("EXCEPTION: ", e.toString());
		}
		return "ERROR";
	}
	
	public ArrayList<String> readSystemFileArray(String file) {
		InputStream in = null;
		try {
			Process p = new ProcessBuilder(new String[] { "cat", file }).start();
			in = p.getInputStream();
			ArrayList<String> contents = readAllArray(in);
			return contents;
		}
		catch (Exception e) {
			//do something
		}
		return null;
	}
		
	public ArrayList<String> readAllArray(InputStream in) {
		ArrayList<String> values = new ArrayList<String>();
		Scanner scan = new Scanner(in);
		while(scan.hasNextLine()) {
			values.add(scan.nextLine());
		}
		return values;
	}
	
	public int getCurrentFrequency(String file) {
		InputStream in = null;
		try {
			Process p = new ProcessBuilder(new String[] { "cat", file }).start();
			in = p.getInputStream();
			String current = readAll(in);
			return Integer.parseInt(current);
		} 
		catch (Exception e) {
			//do something
		}
		return 0;
	}
	
	public String readAll(InputStream in) {
		StringBuilder sBuilder = new StringBuilder();
		Scanner scan = new Scanner(in);
		while(scan.hasNextLine()) {
			sBuilder.append(scan.nextLine());
		}
		return sBuilder.toString();
	}
	
	public void writeToFile(String s) {
		Process p = null;
		try {
			p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			os.writeBytes(s + "\n");
			os.flush();
			os.writeBytes("mount -o remount ro /system\n");
			os.flush();
			//p.waitFor();
			//p.destroy();
		}
		catch (Exception e) {
			p = null;
		}
	}
	
	
	
}
