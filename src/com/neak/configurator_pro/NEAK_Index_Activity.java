package com.neak.configurator_pro;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class NEAK_Index_Activity extends Activity {

	/** Called when the activity is first created. */
	private static boolean isNeak = false;
	private static boolean isSThree = false;
	
	Button btnOptions;
	Button btnDownloader;
	Button btnCPU;
	Button btnGPU;
	Button btnVoltage;
	Button btnScreen;
	Button btnMisc;
	Button btnIntVolt;
	
	Button btBackup, btRestore;
	
	NEAK_CPU_Control_Activity CPU;
	NEAK_GPU_Control_Activity GPU;
	NEAK_GPU_S3_Control_Activity GPUS3;
	NEAK_Misc_Options_Activity MISC;
	NEAK_Misc_Options_S3_Activity MISCS3;
	NEAK_Screen_Control_Activity SCREEN;
	NEAK_Voltage_Control_Activity VOLT;
	NEAK_Internal_Voltages_Activity INTVOLT;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.neak_index_layout);
	    
	    btnOptions = (Button) findViewById(R.id.button_options);
	    btnDownloader = (Button) findViewById(R.id.button_Downloader);
	    btnCPU = (Button) findViewById(R.id.button_CPU);
	    btnGPU = (Button) findViewById(R.id.button_GPU);
	    btnVoltage = (Button) findViewById(R.id.button_Voltage);
	    btnScreen = (Button) findViewById(R.id.button_Screen);
	    btnMisc = (Button) findViewById(R.id.button_Misc);
	    btnIntVolt = (Button) findViewById(R.id.button_IntVolt);
	    btBackup = (Button) findViewById(R.id.bt_backup_settings);
	    btRestore = (Button) findViewById(R.id.bt_restore_settings);
	    
	    if (initiateButtons() == false) {
	    	Toast.makeText(this, "Buttons not initiated", Toast.LENGTH_LONG).show();
	    }
	    
	    CPU = new NEAK_CPU_Control_Activity();
		GPU = new NEAK_GPU_Control_Activity();
		GPUS3 = new NEAK_GPU_S3_Control_Activity();
		MISC = new NEAK_Misc_Options_Activity();
		MISCS3 = new NEAK_Misc_Options_S3_Activity();
		SCREEN = new NEAK_Screen_Control_Activity();
		VOLT = new NEAK_Voltage_Control_Activity();
		INTVOLT = new NEAK_Internal_Voltages_Activity();
	
	    // TODO Auto-generated method stub
	}
	
	public boolean initiateButtons() {
		if (isNeak) {
			if (isSThree) {
				btnOptions.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(1);
		            }
		        });
				btnDownloader.setVisibility(View.GONE);
				btnCPU.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(2);
		            }
		        });
				btnGPU.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(3);
		            }
		        });
				btnVoltage.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(4);
		            }
		        });
				btnIntVolt.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(5);
		            }
		        });
				btnScreen.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(6);
		            }
		        });
				btnMisc.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(7);
		            }
		        });
				btBackup.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                backupS3();
		            }
		        });
				btRestore.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                restoreS3();
		            }
		        });
			}
			else {
				btnOptions.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(1);
		            }
		        });
				btnDownloader.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(2);
		            }
		        });
				btnCPU.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(3);
		            }
		        });
				btnGPU.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(4);
		            }
		        });
				btnVoltage.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(5);
		            }
		        });
				btnIntVolt.setVisibility(View.GONE);
				btnScreen.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(6);
		            }
		        });
				btnMisc.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                NEAK_Configurator_Activity.tabHost.setCurrentTab(7);
		            }
		        });
				btBackup.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                backupFull();
		            }
		        });
				btRestore.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		                restoreFull();
		            }
		        });
			}
		}
		else {
			btnOptions.setVisibility(View.GONE);
			btnDownloader.setVisibility(View.GONE);
			btnCPU.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	                NEAK_Configurator_Activity.tabHost.setCurrentTab(1);
	            }
	        });
			btnGPU.setVisibility(View.GONE);
			btnVoltage.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	                NEAK_Configurator_Activity.tabHost.setCurrentTab(2);
	            }
	        });
			btnIntVolt.setVisibility(View.GONE);
			btnScreen.setVisibility(View.GONE);
			btnMisc.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	                NEAK_Configurator_Activity.tabHost.setCurrentTab(3);
	            }
	        });
			btBackup.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	                backupLite();
	            }
	        });
			btRestore.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	                restoreLite();
	            }
	        });
		}
		
		return true;
	}
	
	public static void setNeakKernel(boolean nk) {
		isNeak = nk;
	}
	
	public static void setSThree(boolean S3) {
		isSThree = S3;
	}
	
	public void backupLite() {
		File backupDir = new File("/data/neak/backup");
		File f;
		Process p;
		try {
			p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			if (!(backupDir.isDirectory())) {
				if (!(backupDir.mkdirs())) {
					os.writeBytes("mkdir /data/neak/backup\n");
					os.flush();
				}
			}
			else if (backupDir.isDirectory()) {
				os.writeBytes("rm /data/neak/backup/*\n");
				os.flush();
				os.writeBytes("rmdir /data/neak/backup\n");
				os.flush();
				os.writeBytes("mkdir /data/neak/backup\n");
				os.flush();
				os.writeBytes("chmod -R 777 /data/neak/backup\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S40neakCPU");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S40neakCPU /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S42neakVoltage");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S42neakVoltage /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S44neakMisc");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S44neakMisc /data/neak/backup/\n");
				os.flush();
			}
			Toast.makeText(this, "Backup Created", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//CPU.backupSettings();
		//VOLT.backupSettings();
		//MISC.backupSettings();
	}
	
	public void restoreLite() {
		File backupDir = new File("/data/neak/backup");
		File f;
		Process p;
		try {
			p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			if (backupDir.isDirectory()) {
				f = new File("/data/neak/backup/S40neakCPU");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S40neakCPU\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S40neakCPU system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S42neakVoltage");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S42neakVoltage\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S42neakVoltage system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S44neakMisc");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S44neakMisc\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S44neakMisc system/etc/init.d/\n");
					os.flush();
				}
				Toast.makeText(this, "Backup Restored", Toast.LENGTH_SHORT).show();
				Runtime.getRuntime().exec("su -c reboot");
			} else {
				Toast.makeText(this, "No Backup To Restore", Toast.LENGTH_SHORT).show();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//CPU.restoreSettings();
		//VOLT.restoreSettings();
		//MISC.restoreSettings();
	}
	
	public void backupFull() {
		File backupDir = new File("/data/neak/backup");
		File f;
		Process p;
		try {
			p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			if (!(backupDir.isDirectory())) {
				if (!(backupDir.mkdirs())) {
					os.writeBytes("mkdir /data/neak/backup\n");
					os.flush();
				}
			}
			else if (backupDir.isDirectory()) {
				os.writeBytes("rm /data/neak/backup/*\n");
				os.flush();
				os.writeBytes("rmdir /data/neak/backup\n");
				os.flush();
				os.writeBytes("mkdir /data/neak/backup\n");
				os.flush();
				os.writeBytes("chmod -R 777 /data/neak/backup\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S40neakCPU");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S40neakCPU /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S41neakGPU");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S41neakGPU /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S42neakVoltage");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S42neakVoltage /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S43neakScreen");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S43neakScreen /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S44neakMisc");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S44neakMisc /data/neak/backup/\n");
				os.flush();
			}
			Toast.makeText(this, "Backup Created", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//CPU.backupSettings();
		//VOLT.backupSettings();
		//MISC.backupSettings();
		//GPU.backupSettings();
		//SCREEN.backupSettings();
	}
	
	public void restoreFull() {
		File backupDir = new File("/data/neak/backup");
		File f;
		Process p;
		try {
			p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			if (backupDir.isDirectory()){
				f = new File("/data/neak/backup/S40neakCPU");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S40neakCPU\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S40neakCPU system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S41neakGPU");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S41neakGPU\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S41neakGPU system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S42neakVoltage");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S42neakVoltage\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S42neakVoltage system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S43neakScreen");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S43neakScreen\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S43neakScreen system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S44neakMisc");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S44neakMisc\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S44neakMisc system/etc/init.d/\n");
					os.flush();
				}
				Toast.makeText(this, "Backup Restored", Toast.LENGTH_SHORT).show();
				Runtime.getRuntime().exec("su -c reboot");
			} else {
				Toast.makeText(this, "No Backup To Restore", Toast.LENGTH_SHORT).show();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//CPU.restoreSettings();
		//VOLT.restoreSettings();
		//MISC.restoreSettings();
		//GPU.restoreSettings();
		//SCREEN.restoreSettings();
	}
	
	public void backupS3() {
		File backupDir = new File("/data/neak/backup");
		File f;
		Process p;
		try {
			p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			if (!(backupDir.isDirectory())) {
				if (!(backupDir.mkdirs())) {
					os.writeBytes("mkdir /data/neak/backup\n");
					os.flush();
				}
			}
			else if (backupDir.isDirectory()) {
				os.writeBytes("rm /data/neak/backup/*\n");
				os.flush();
				os.writeBytes("rmdir /data/neak/backup\n");
				os.flush();
				os.writeBytes("mkdir /data/neak/backup\n");
				os.flush();
				os.writeBytes("chmod -R 777 /data/neak/backup\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S40neakCPU");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S40neakCPU /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S41neakGPU");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S41neakGPU /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S42neakVoltage");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S42neakVoltage /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S43neakScreen");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S43neakScreen /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S44neakMisc");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S44neakMisc /data/neak/backup/\n");
				os.flush();
			}
			f = new File("system/etc/init.d/S45neakIntVolt");
			if (f.exists()) {
				os.writeBytes("cp system/etc/init.d/S45neakIntVolt /data/neak/backup/\n");
				os.flush();
			}
			Toast.makeText(this, "Backup Created", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//CPU.backupSettings();
		//VOLT.backupSettings();
		//MISCS3.backupSettings();
		//GPUS3.backupSettings();
		//SCREEN.backupSettings();		
	}
	
	public void restoreS3() {
		File backupDir = new File("/data/neak/backup");
		File f;
		Process p;
		try {
			p = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			os.writeBytes("mount -o remount rw /system\n");
			os.flush();
			if (backupDir.isDirectory()){
				f = new File("/data/neak/backup/S40neakCPU");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S40neakCPU\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S40neakCPU system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S41neakGPU");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S41neakGPU\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S41neakGPU system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S42neakVoltage");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S42neakVoltage\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S42neakVoltage system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S43neakScreen");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S43neakScreen\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S43neakScreen system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S44neakMisc");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S44neakMisc\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S44neakMisc system/etc/init.d/\n");
					os.flush();
				}
				f = new File("/data/neak/backup/S45neakIntVolt");
				if (f.exists()) {
					os.writeBytes("rm system/etc/init.d/S45neakIntVolt\n");
					os.flush();
					os.writeBytes("cp /data/neak/backup/S45neakIntVolt system/etc/init.d/\n");
					os.flush();
				}
				Toast.makeText(this, "Backup Restored", Toast.LENGTH_SHORT).show();
				Runtime.getRuntime().exec("su -c reboot");
			} else {
				Toast.makeText(this, "No Backup To Restore", Toast.LENGTH_SHORT).show();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//CPU.restoreSettings();
		//VOLT.restoreSettings();
		//MISCS3.restoreSettings();
		//GPUS3.restoreSettings();
		//SCREEN.restoreSettings();		
	}

}
